#pragma once
#ifdef MVE_LUA_ENABLED

#include "lua.hpp"
#include <vector>
#include <iostream>
#include <stdexcept>
#include <luawrapper.hpp>
#include <functional>
#include <LuaBridge/LuaBridge.h>

namespace mve {
namespace lua {

    /**
     * @brief The LuaException class
     */
    struct LuaException : public std::logic_error
    {

        LuaException(const std::string &msg)
            : std::logic_error(msg)
        {
        }
    };

    /**
     *
     */
    template <typename T>
    T getFromStack(lua_State* L)
    {
        std::string ret;
        if (!lua_isnil(L, -1)) {
            ret = lua_tostring(L, -1);
        } else {
            throw LuaException("Value on the stack is nil");
        }

        return ret;
    }

    /**
     *
     */
    template <typename T>
    T getTableField(lua_State* L, std::string const& name)
    {
        std::string::size_type startPos = 0;
        std::string::size_type endPos = name.find(".", startPos);
        std::size_t stackCount = 0;

        T ret;

        if (std::string::npos == endPos) {
            lua_getglobal(L, name.c_str());

            ret = getFromStack<T>(L);
            lua_pop(L, 1);
            return ret;
        }

        lua_getglobal(L, name.substr(startPos, endPos - startPos).c_str());
        startPos = endPos + 1;
        ++stackCount;

        while (std::string::npos != (endPos = name.find(".", startPos))) {
            std::string tableName = name.substr(startPos, endPos - startPos);

            if (!lua_istable(L, -1)) {
                lua_pop(L, stackCount);
                throw LuaException("Invalid variable " + name);
            }

            lua_getfield(L, -1, tableName.c_str());
            ++stackCount;
            startPos = endPos + 1;
        }

        if (!lua_istable(L, -1)) {
            lua_pop(L, stackCount);
            throw LuaException("Invalid variable " + name);
        }

        lua_getfield(L, -1, name.substr(startPos).c_str());
        ++stackCount;

        ret = getFromStack<T>(L);

        lua_pop(L, stackCount);
        return ret;
    }

    template <typename T, typename... Args>
    T* default_allocator(lua_State* L, Args... args) {
        return new T(args...);
    }

    template <typename T>
    void default_deallocator(lua_State* L, T* obj) {
        delete obj;
    }

    template <typename T>
    void default_identifier(lua_State* L, T* obj) {
        lua_pushlightuserdata(L, static_cast<void*>(obj));
    }


    /**
     * @brief The LuaVM class
     */
    class LuaVM
    {
    public:
        using TableCallback = std::function<void(LuaVM& vm)>;

    public:
        LuaVM();
        LuaVM(LuaVM&& other);

        ~LuaVM();

        LuaVM(const LuaVM& vm) = delete;
        LuaVM& operator=(const LuaVM& other) = delete;


    public:
        /**
         * @brief L
         * @return
         */
        lua_State* L() {
            return mL;
        }

        /**
         * @brief operator *
         * @return
         */
        lua_State* operator* () {
            return mL;
        }

        /**
         * @brief load
         */
        void load();

        /**
         * @brief reload
         */
        void reload();

        template <typename T, typename... Args>
        void registerClass(const char* name, const luaL_Reg* table,
                           const luaL_Reg* metaTable, Args... args)
        {
            luaW_register<T>(mL, name, table, metaTable, args...);
        }

        /**
         * @brief registerScript
         * @param filename
         */
        void registerScript(const char* filename);

    public:

        template <typename T>
        void addTableField(const char* name, T value)
        {
            luabridge::push(mL, value);
            lua_setfield(mL, -2, name);
        }

        void createGlobalTable(const char* name, bool push = true);
        void createTable(const char* name, TableCallback cb = TableCallback());

        // Various getters
        template <typename T>
        T getTableField(std::string const& name) {
            return lua::getTableField<T>(mL, name);
        }

        void popStack(int count = 1) {
            lua_pop(mL, count);
        }

        void pushGlobalTable(const char *name) {
            lua_getglobal(mL, name);
        }

    private:
        void init();
        void logErrors(int state);

    private:
        lua_State*                      mL;
        std::vector<const char*>        mScripts;
    };
} /* namespace lua */
} /* namespace mve */

#endif // MVE_LUA_ENABLED
