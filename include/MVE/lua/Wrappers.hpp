#pragma once

#if MVE_LUA_ENABLED
#include <MVE/lua/LuaVM.hpp>

namespace mve
{
    namespace lua
    {
        void initialize_data_table(LuaVM& vm);

    }
}

#endif
