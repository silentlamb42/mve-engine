#pragma once

#ifdef MVE_LUA_ENABLED
#include <functional>

namespace mve {

    typedef std::function<void()> Callback;

    /**
     * @brief The LuaScriptListener class
     */
    class LuaScriptListener
    {
    public:
        /**
         * @brief LuaScriptListener
         * @param onLoad
         * @param onReload
         */
        LuaScriptListener(Callback onLoad, Callback onReload);

    public:
        /**
         * @brief onScriptLoad
         */
        void onScriptLoad();

        /**
         * @brief onScriptReload
         */
        void onScriptReload();

    private:
        Callback mOnLoadCallback;
        Callback mOnReloadCallback;
    };

}
#endif // MVE_LUA_ENABLED
