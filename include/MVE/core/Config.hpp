#pragma once

#include <cstddef>
#include <cstdint>

// TODO: Class probably not needed anymore, relic of the past

// Move to Types.hpp

typedef unsigned int    uint;   // At least 16 bits
typedef unsigned long   ulong;  // At least 32 bits

namespace mve
{
    namespace config
    {
        namespace debug
        {
            constexpr bool DisplayFps = false;

#ifdef LOGME_TRACE_CALLS
            const bool     LogDisplayFunctionCalls = true;
#else
            constexpr bool LogDisplayFunctionCalls = false;
#endif

            constexpr bool LogUseAnsiColors = false;
        }
    }
}
