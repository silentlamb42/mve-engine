#pragma once

#include <MVE/core/Types.hpp>

namespace mve {
namespace def {


    namespace state
    {

        constexpr MvUint MaxStates = 255;

        enum Id {
            Default = 0,
            Invalid = MaxStates,
        };
    }

    // TODO: Remove hardcoded number of layers, store them in a sorted map
    namespace layer
    {
        enum Id {
            Bottom              = 0,
            BackgroundFar       = 1,
            BackgroundNear      = 2,

            ForegroundBottom    = 3,
            ForegroundMiddle    = 4,
            ForegroundTop       = 5,

            Overlay             = 6,
            Gui                 = 7,

            // Always last, do not use this
            Count
        };

        constexpr MvLayerId Default = ForegroundMiddle;
    }

    namespace direction
    {
        enum Id {
            Unknown     = 0,
            North       = 1,
            East        = 2,
            South       = 4,
            West        = 8,

            NorthEast   = North + East, // 3
            SouthEast   = South + East, // 6
            NorthWest   = North + West, // 9
            SouthWest   = South + West, // 12
        };
    }

} // namespace def
} // namespace mve
