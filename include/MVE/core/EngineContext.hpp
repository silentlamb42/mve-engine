#pragma once

#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif
#include <MVE/res/ResourceManager.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <memory>

namespace sf
{
    class RenderWindow;
}

namespace mve
{

    /**
     * @brief Context passed between many different layers to provide access to resources, window,
     *        camera and other objects of the engine's context.
     */
    class EngineContext : public sf::NonCopyable
    {
    public:

        using Ptr = std::unique_ptr<EngineContext>;

    public:
        EngineContext(sf::RenderWindow& window);
        ~EngineContext() = default;

    public:

        /**
         * @brief Return window for rendering
         *
         * TODO: Implement post-effects and change RenderWindow to RenderTarget
         *
         * @return
         */
        sf::RenderWindow*   getWindow()
        {
            return mWindow;
        }

        /**
         * @brief Return resource manager used to load, unload and retrieve various resources
         *
         * @return
         */
        ResourceManager& getResourceManager()
        {
            return mResourceManager;
        }

#ifdef MVE_LUA_ENABLED
        lua::LuaVM&         getLuaVM();
#endif

    private:
        sf::RenderWindow*   mWindow;
        ResourceManager     mResourceManager;
#ifdef MVE_LUA_ENABLED
        lua::LuaVM          mLuaVM;
#endif

    };
}


