#pragma once

#include <cstdint>
#include <cstddef>

typedef unsigned int    MvUint;
typedef unsigned long   MvUlong;

typedef MvUlong         MvId;
typedef std::size_t     MvSize;

using MvResId               = MvUint;
using MvSceneId             = MvUint;

using EntityCategory        = signed int;
using EntityCategoryMask    = MvUint;
using EntityGroup           = MvUint;

using MvSensorCategory      = MvUint;
using MvStateId             = MvUint;
using MvLayerId             = MvUint;
using MvDirection           = MvUint;
