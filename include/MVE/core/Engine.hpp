#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>
#include <fstream>
#include <memory>
#include <string>


namespace sf
{
    class Event;
    class RenderWindow;
}

namespace mve
{
    // Forward declarations
    class EngineContext;
    class Director;

    // Aliasing
    using WindowPtr = std::unique_ptr<sf::RenderWindow>;
    using ContextPtr = std::unique_ptr<mve::EngineContext>;
    using DirectorPtr = std::unique_ptr<mve::Director>;


    /**
     * @brief Base class of the engine
     *
     * Provides common engine functionality. Derived classes might just override the on*
     * abstract methods and hide all engine internals like main loop in a base class.
     *
     * TODO: Documentation
     */
    class Engine : private sf::NonCopyable
    {
    public:
        typedef std::unique_ptr<Engine>     Ptr;

    public:
        Engine(WindowPtr window, ContextPtr context, DirectorPtr Director);
        virtual         ~Engine();

    public:
        void                cleanup();
        void                exec();
        bool                parseArgs(int argc, char** argv);

        virtual std::string getVersion() const = 0;

    public:
        bool                getDisplayFps() const { return mDisplayFps; }
        void                setDisplayFps(bool enabled);

    protected:
        /**
        * @brief onEventHandle
        * @param event
        * @return false if event should not be processed
        */
        virtual bool        onEventHandle(const sf::Event& event) { return true; }
        virtual void        onFpsUpdate(std::size_t fps) {}
        virtual bool        onInitResources() = 0;
#ifdef MVE_LUA_ENABLED
        virtual bool        onInitLua() = 0;
        virtual bool        onInitScripts() = 0;
#endif
        virtual bool        onInitDirector() = 0;
        virtual bool        onInitWindow() = 0;
        virtual void        onLogicUpdate(sf::Time dt) {}
        virtual void        onLoopFinished() {}
        virtual void        onLoopStarted() {}
        virtual void        onRenderFinished() {}
        virtual void        onRenderStarted() {}

    protected:
        sf::RenderWindow&   getWindow() { return *mWindow; }
        EngineContext&      getEngineContext() { return *mContext; }
        Director&           getDirector() { return *mDirector; }

    private:
        bool                initResources();
        bool                initScripts();
        bool                initDirector();
        bool                initWindow();
        bool                initEngine();

    private:
        struct LogOptions {
            bool            toFile;
            std::ofstream   stream;
        };

    private:
        WindowPtr   mWindow;
        ContextPtr  mContext;
        DirectorPtr mDirector;
        LogOptions  mOpts;
        sf::Color   mBgColor {210, 210, 210};

        bool        mDisplayFps {true};
    };

} // namespace mve
