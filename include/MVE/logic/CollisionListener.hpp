#pragma once

#include <MVE/entity/Entity.hpp>
class b2Contact;

// TODO: Documentation

namespace mve
{

    /**
     * @brief The CollisionListener class
     */
    class CollisionListener
    {
    public:

        /**
         * @brief onSensorContactBegin
         * @param eSensor
         * @param eTarget
         * @return
         */
        virtual bool onSensorContactBegin(b2Contact* contact, Entity eSensor, Entity eTarget) = 0;

        /**
         * @brief onSensorContactEnd
         * @param eSensor
         * @param eTarget
         * @return
         */
        virtual bool onSensorContactEnd(b2Contact* contact, Entity eSensor, Entity eTarget) = 0;

        /**
         * @brief onContactBegin
         * @param contact
         * @return
         */
        virtual bool onRigidBodyContactBegin(b2Contact* contact, Entity eSensor, Entity eTarget) = 0;

        /**
         * @brief onContactEnd
         * @param contact
         * @return
         */
        virtual bool onRigidBodyContactEnd(b2Contact* contact, Entity eSensor, Entity eTarget) = 0;
    };

}
