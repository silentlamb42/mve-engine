#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/entity/Component.hpp>
#include <SFML/System/Time.hpp>

namespace mve
{
#ifdef MVE_LUA_ENABLED
    namespace lua
    {
        class LuaVM;
    }
#endif
}


namespace mve
{
    constexpr MvStateId MaxStateNum = 256;


    class State : public Component<State>
    {
    public:
#ifdef MVE_LUA_ENABLED
        static void registerLuaContext(mve::lua::LuaVM& vm);
#endif

    public:

        State()
            : mState {}
            , mTimer{sf::Time::Zero}
        {
        }

        ~State()
        {
        }

        /**
         * @return Current state id
         */
        MvStateId getState() const
        {
            return mState;
        }

        /**
         * @return  True if state is active for a given amount of time
         */
        bool isExpired(sf::Time time) const
        {
            return mTimer >= time;
        }

        /**
         * @brief Change current state
         * @param id    new state
         */
        void setState(MvStateId id)
        {
            LOGME_CALL_SILENT("State::setState");
            LOGME_ASSERT(Invalid() > id, "Invalid state: %d", id);
            if (id != mState) {
                LOGME_DEBUG("Switching state %d -> %d [length: %d ms]",
                            id, mState, mTimer.asMilliseconds());
                mState = id;
                mTimer = sf::Time::Zero;
            }
        }

        /**
         * @brief Update timer internals
         *
         * @param dt    delta time that's been passed
         */
        void updateTimer(sf::Time dt)
        {
            mTimer += dt;
        }

        /**
         * @brief Id of the invalid state
         * @return
         */
        static constexpr MvStateId Invalid()
        {
            return MaxStateNum;
        }

        /**
         * @brief Id of the default (0) state
         * @return
         */
        static constexpr MvStateId Default()
        {
            return 0;
        }

    private:
            MvStateId mState;
            sf::Time  mTimer;
    };


    std::ostream& operator<<(std::ostream& os, const State & state);
}
