#pragma once

#include <Box2D/Box2D.h>

#include <MVE/components/CollisionComponent.hpp>
#include <MVE/components/ShapeComponent.hpp>
#include <MVE/core/Types.hpp>
#include <MVE/entity/Component.hpp>
#include <MVE/entity/Entity.hpp>


#include <SFML/System/Vector2.hpp>

#include <vector>


namespace mve
{
    /**
     * @brief Sensor component to detect other objects within a given area.
     *
     *
     * TODO: Design component so it might have more than one sensor
     */
    class Sensor
        : public Component<Sensor>
        , public priv::CollisionComponent
        , public priv::ShapeComponent
    {
    public:

        Sensor()
            : priv::CollisionComponent()
            , priv::ShapeComponent()
            , activated {false}
            , attachToEntity {false}
            , sensorCategory {}
            , fixture {nullptr}
            , fixtureDef {}
            , seen {}
            , mWorld {nullptr}
        {
            fixtureDef.density              = 0;
            fixtureDef.filter.categoryBits  = mCategory;
            fixtureDef.filter.groupIndex    = mGroup;
            fixtureDef.filter.maskBits      = mMask;
            fixtureDef.friction             = 0;
            fixtureDef.restitution          = 0;
            fixtureDef.isSensor             = true;
            fixtureDef.shape                = nullptr;
        }

        ~Sensor()
        {
            // Unregister fixture if registered
            if (nullptr != mWorld) {
                // One way to fix issue of having fixture already destroyed is to rearrange
                // RigidBody's and Sensor's reset() calls. This hides the problem.
                //
                // The other way is to iterate through the fixtures of the body to see
                // if the fixture already got destroyed. This is independent of RigidBody's
                // destruction.

                b2Body* body = fixture->GetBody();
                b2Fixture* fixture = body->GetFixtureList();
                while (fixture) {
                    if (fixture == this->fixture) {
                        body->DestroyFixture(fixture);
                        break;
                    }
                }

                mWorld = nullptr;
            }

            this->fixture = nullptr;
        }

    public:

        /**
         * @brief Mark entity as sensed by this sensor
         *
         * @param entity    entity being sensed
         */
        void markSeen(Entity entity);

        /**
         * @brief Mark entity as sensed no more by this sensor
         *
         * @param entity    entity being sensed no more
         */
        void markUnseen(Entity entity);

        /**
         * @brief Clear state of the sensor to not sense anything
         */
        void unseeAll()
        {
            seen.clear();
        }

        /**
         * @brief If set sensor will be attached to the RigidBody of the entity having this sensor.
         *
         * Otherwise, sensor will be attached to some global body by physics engine.
         *
         */
        void setAttachToEntity()
        {
            attachToEntity = true;
        }

        /**
         * @brief Sets category of the sensor
         *
         * This is separate from the collision category.
         *
         * @param c     sensor category to set
         */
        void setCategory(MvSensorCategory c)
        {
            sensorCategory = c;
        }

        /**
         * @brief Test whether the sensor is of given category
         *
         * @param c     sensor category for test
         */
        bool isCategory(MvSensorCategory c)
        {
            return c == sensorCategory;
        }

        /**
         * @brief Called by a system to finalize setting up physics engine
         *
         * This creates sensor fixture and attaches it to the given @a sensorBody.
         *
         * b2World pointer is used to unregister fixture when the component is reset.
         *
         * @param entity        entity of the sensor
         * @param world         Box2D current context
         * @param sensorBody    Box2D body for fixture attachement
         */
        void registerFixture(Entity entity, b2World* world, b2Body* sensorBody)
        {
            LOGME_CALL_SILENT("Sensor::registerFixture");
            LOGME_ASSERT(nullptr == fixture,          "Fixture already registered");
            LOGME_ASSERT(nullptr == fixtureDef.shape, "Shape already attached");

            fixtureDef.userData = reinterpret_cast<void*>(entity);

            attachCollision(fixtureDef);
            attachShape(fixtureDef);

            fixture = sensorBody->CreateFixture(&fixtureDef);

            mWorld = world;
        }


    public:
        bool                activated;
        bool                attachToEntity;
        MvSensorCategory    sensorCategory;
        b2Fixture*          fixture;
        b2FixtureDef        fixtureDef;
        std::vector<Entity> seen;
        b2World*            mWorld;
    };
}
