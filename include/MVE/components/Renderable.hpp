#pragma once

#include <MVE/core/Definitions.hpp>
#include <MVE/core/Types.hpp>
#include <MVE/entity/Component.hpp>
#include <MVE/render/MvDrawable.hpp>
#include <MVE/render/StateDrawable.hpp>

#include <memory>

namespace mve
{
    /**
     * @brief Type of drawable (corresponding to the known drawable classes
     */
    enum class DrawableType
    {
        Default,        /*< Default:        can be stored as MvDrawable */
        State,          /*< State:          drawables bound to states */
        Directional,    /*< Directional:    drawables bound to both states and direction */
    };

    /**
     * @brief Convert DrawableType enum to string value for debugging purposes
     *
     * @param type  type of drawable
     * @return      string representation
     */
    inline std::string toString(DrawableType type)
    {
        if (type == DrawableType::Default) {
            return "Default";
        }
        else if (type == DrawableType::State) {
            return "State";
        }
        else if (type == DrawableType::Directional) {
            return "Directional";
        }
        else {
            return "Unknown";
        }
    }

    /**
     * @brief Renderable component is used to draw the entity on the screen
     *
     * Renderable component is characterized by
     * - a layer on which the entity is drawn,
     * - drawable or set of drawables that handles rendering on the render target
     * - visibility status (hide/show)
     *
     * API of drawables used in the renderable component requires to have a position
     * on the scene in order to draw it properly. This means that some System needs
     * to synchronize Transform component with the renderable component.
     *
     * TODO:
     * - Shaders
     * - Less strict layers
     * - Camera (or maybe camera bound to the whole layer?)
     */
    class Renderable : public Component<Renderable>
    {
    public:

        Renderable()
            : mLayerId {def::layer::Default}
            , mVisible {true}
            , mDrawableType {DrawableType::Default}
        {
            // NOOP
        }

        ~Renderable()
        {
            // NOOP
        }

        /**
         * @brief Return instance of StateDrawable for the entity
         *
         * Note: Make sure setDrawable won't be called when using reference from this method!
         * The drawable is guarded by a unique_ptr and setDrawable method destroys the old object!
         */
        MvDrawable& getDrawable()
        {
            return *mDrawable;
        }

        /**
         * @brief Return type of drawable that's being hold
         *
         * Used for downcasting purposes.
         *
         * @return
         */
        DrawableType getDrawableType() const
        {
            return mDrawableType;
        }

        /**
         * @brief Layer id on which the entity is drawn
         *
         * Used for ordering purposes
         *
         * @return
         */
        MvLayerId getLayer() const
        {
            return mLayerId;
        }

        /**
         * @brief True if drawable might be drawn differently, depending on the entity's direction
         *
         * Might be used to synchronize Direction/Kinematic component with the drawable
         *
         * @return
         */
        bool hasDirections() const
        {
            return mDrawableType == DrawableType::Directional;
        }

        /**
         * @brief True if drawable might be drawn differently, depending on the entity's state
         *
         * Might be used to synchronize State component with the drawable
         *
         * @return
         */
        bool hasStates() const
        {
            return mDrawableType == DrawableType::Directional || mDrawableType == DrawableType::State;
        }

        /**
         * @brief Return visibility state
         *
         * @return true = visible, false = hidden
         */
        bool isVisible() const
        {
            return mVisible;
        }

        /**
         * @brief Move drawable to this component and mark it as a drawable of given type
         *
         * @param ptr
         * @param type
         */
        void setDrawable(MvDrawable::Ptr ptr, DrawableType type = DrawableType::Default)
        {
            mDrawable = std::move(ptr);
            mDrawableType = type;
        }

        /**
         * @brief Set id of the layer on which the entity is drawn
         *
         * @param layerId
         */
        void setLayer(MvLayerId layerId)
        {
            LOGME_CALL_SILENT("Renderable::setLayer");
            LOGME_ASSERT(layerId < def::layer::Count,
                "Layer out of bounds (is: %d, max allowed: %d)",
                layerId, def::layer::Count);

            mLayerId = layerId;
        }

        /**
         * @brief Mark entity as visible/hidden
         *
         * @param visible   true if visible, false if hidden
         */
        void setVisible(bool visible)
        {
            mVisible = visible;
        }


    private:
        MvLayerId           mLayerId;
        bool                mVisible;
        DrawableType        mDrawableType;
        MvDrawable::Ptr     mDrawable;
    };
}
