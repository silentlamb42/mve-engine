#pragma once

#include <MVE/core/Definitions.hpp>
#include <MVE/core/Types.hpp>
#include <MVE/entity/Component.hpp>
#include <MVE/utils/Helpers.hpp>
#include <SFML/System/Vector2.hpp>

#ifdef MVE_LUA_ENABLED
namespace mve
{
    namespace lua
    {
        class LuaVM;
    }
}
#endif

namespace mve
{
    /**
     * @brief Component that handles movement, forces and stores direction.
     */
    class Kinematic : public Component<Kinematic>
    {
    public:
#ifdef MVE_LUA_ENABLED
        static void registerLuaContext(mve::lua::LuaVM& vm);
#endif

        Kinematic();
        ~Kinematic();

    public:

        /**
         * @brief Apply angular impulse
         *
         * @param impulse in kg*m*m/s
         */
        void applyAngularImpulse(float impulse)
        {
            mImpulseAngular = impulse;
        }

        /**
         * @brief Apply linear impulse to the center of mass
         *
         * @param impulse in kg * m/s
         */
        void applyLinearImpulse(sf::Vector2f impulse)
        {
            mImpulseLinear = impulse;
        }

        /**
         * @brief Return angular velocity
         *
         * @return angular velocity in kg*m*m/s
         */
        float getAngularVelocity() const
        {
            return mVelocityAngular;
        }

        /**
         * @brief Return angular impulse
         *
         * @return angular impulse in  kg*m*m/s
         */
        float getAngularImpulse() const
        {
            return mImpulseAngular;
        }

        /**
         * @brief Return linear impulse vector
         */
        sf::Vector2f getLinearImpulse() const
        {
            return mImpulseLinear;
        }

        /**
         * @brief Return linear velocity vector
         * @return linear velocity vector, units: m/s
         */
        sf::Vector2f getLinearVelocity() const
        {
            return mVelocityLinear;
        }

        /**
         * @brief Set angular velocity
         *
         * @param velocity in kg * m * m/s
         */
        void setAngularVelocity(float velocity)
        {
            mVelocityAngular = velocity;
        }

        /**
         * @brief Set linear velocity vector
         *
         * @param velocity vector, units: m/s
         */
        void setLinearVelocity(sf::Vector2f velocity)
        {
            mVelocityLinear = velocity;
        }

        // TODO: Move direction methods out to Direction component

        /**
         * @brief Set direction constant
         *
         * The meaning of values are not specified
         *
         * @param direction
         */
        void setDirection(MvDirection direction)
        {
            mDirection = direction;
        }

        /**
         * @brief Return direction constant
         *
         * @return
         */
        MvDirection getDirection() const
        {
            return mDirection;
        }

    private:
        MvId            mDirection;
        float           mImpulseAngular;
        float           mVelocityAngular;
        sf::Vector2f    mImpulseLinear;
        sf::Vector2f    mVelocityLinear;
    };
}
