#pragma once

#include <MVE/core/Types.hpp>


class b2FixtureDef;

namespace mve
{

    /**
     * @brief Enum related to collisions
     */
    namespace Collision
    {
        constexpr MvUint NoCategory = 0;

        /**
         * @brief Mask constants for collision filters
         */
        enum Mask {
            MaskNone = 0x00000,  /**< Collision with nothing */
            MaskAll  = 0xFFFFF,  /**< Collision with everything  (16 bits)*/
        };

        /**
         * @brief Collision group behavior constants
         */
        enum Behavior {
            Disabled      = 1,  /**< No group collision */
            AlwaysCollide = 2,  /**< Objects of the same group always collide (ignore filter mask) */
            NeverCollide  = 4,  /**< Objects of the same group never collide (ignore filter mask) */
        };
    }

    namespace priv
    {

        /**
         * @brief Common interface for components using collision groups and filters
         */
        class CollisionComponent
        {
        public:

            /**
             * @brief Set group collision settings.
             *
             * Collision::Disabled:
             *      Group collision feature is disabled. @a group value is ignored.
             *      With this behavior filter category and mask is used. This is default
             *      settings for newly created object.
             *
             * Collision::AlwaysCollide:
             *      Group collision is enabled. Objects of the same group always collide,
             *      ignoring category/mask rules. Objects of different groups might collide,
             *      depending on category/mask checks.
             *
             * Collision::NeverCollide:
             *      Group collision is enabled. Objects of the same group never collide,
             *      ignoring category/mask rules. Objects of different group might collide,
             *      depending on category/mask checks.
             *
             * @param group Id of the collision group
             * @param type  Collision group behavior
             */
            void setCollisionGroup(EntityGroup group, Collision::Behavior type)
            {
                if (type == Collision::Disabled) {
                    mGroup = 0;
                } else if (type == Collision::AlwaysCollide) {
                    mGroup = group;
                } else if (type == Collision::NeverCollide) {
                    mGroup = -group;
                }
            }

            /**
             * @brief Set collision category to be tested with mask of other object to determine
             * whether two object collide.
             *
             * @param category  Category bit
             */
            void setCollisionCategory(EntityCategory category)
            {
                mCategory = category;
            }

            /**
             * @brief Enable collision with objects of the given categories.
             *
             * @param mask      Categories (bitmask) to enable collision
             */
            void setCollisionWith(EntityCategoryMask mask)
            {
                mMask  |= mask;
            }

            /**
             * @brief reset
             */
            void reset()
            {
                mGroup      = 0;
                mCategory   = Collision::NoCategory;
                mMask       = Collision::MaskNone;
            }

        protected:

            /**
             * @brief Attach collision definitions
             *
             * Under the hood this synchronizes Box2D definition objects (like b2FixtureDef)
             * with local objects (set by CollisionComponent interface)
             *
             * @param fixtureDef
             */
            void attachCollision(b2FixtureDef& fixtureDef);


        protected:
            MvUint mGroup;
            MvUint mCategory;
            MvUint mMask;
        };

    }

}
