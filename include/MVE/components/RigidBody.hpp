#pragma once

#include <MVE/components/CollisionComponent.hpp>
#include <MVE/components/ShapeComponent.hpp>
#include <MVE/core/Types.hpp>
#include <MVE/entity/Entity.hpp>
#include <MVE/entity/Component.hpp>
#include <MVE/utils/Helpers.hpp>
#include <Box2D/Box2D.h>

#include <assert.h>

#ifdef MVE_LUA_ENABLED
namespace mve
{
    namespace lua
    {
        class LuaVM;
    }
}
#endif

namespace mve
{
    // TODO: Move to defaults file
    namespace priv
    {
        constexpr float DefaultFriction     = 0.0f;
        constexpr float DefaultDensity      = 1.0f;
        constexpr float DefaultRestitution  = 0.01f;
    }


    /**
     * @brief The Collider class
     *
     * Remember to update data from Transform component: position and angle.
     * Remember to update data from Kinetic component: velocity and
     *
     * TODO: Move shape methods to separate class
     * TODO: Move Entity* aliases outside this file, to be reused
     */
    class RigidBody
        : public Component<RigidBody>
        , public priv::CollisionComponent
        , public priv::ShapeComponent
    {
    public:
#ifdef MVE_LUA_ENABLED
        static void registerLuaContext(mve::lua::LuaVM &vm);
#endif

    public:

        /**
         * @brief RigidBody
         */
        RigidBody();

        ~RigidBody();

        /**
         * @brief setPosition
         * @param position
         */
        void setPosition(sf::Vector2f position)
        {
            bodyDef.position = units::pixToMetric(position);
        }

        /**
         * @brief Enable collision for the body
         *
         * If fixture already exists in the Box2D world, the previous fixture filter settings
         * are restored. It means that this method might not enable collision at all if settings
         * stored in fixtureDef.filter member did not contain proper values.
         *
         * If Box2D doesn't know about the fixture, it sets default values (previous ones are lost).
         * For default values consider checking b2Fixture.h file.
         *
         * @see disableCollision();
         */
        void enableCollision()
        {
            mEnabled = true;
        }

        /**
         * @brief enableRotation
         */
        void enableRotation()
        {
            setFixedRotation(false);
        }

        /**
         * @brief getMass
         * @return
         */
        float getMass() const
        {
            LOGME_CALL_SILENT("RigidBody::getMass");
            LOGME_ASSERT(nullptr != body, "Physics engine is not running");
            return body->GetMass();
        }

        /**
         * @brief Disable collision for the body
         *
         * If called on non initialized entity (Box2D doesn't know about the fixture)
         * collision parameters like collision category or collision group will be lost
         * pernamently.
         *
         * If fixture already exists the action might be reversed by calling enableCollision().
         */
        void disableCollision();

        /**
         * @brief disableRotation
         */
        void disableRotation()
        {
            setFixedRotation(true);
        }

        /**
         * @brief registerBody
         * @param world
         */
        void registerBody(Entity entity, b2World &world);

        /**
         * @brief setAngularDamping
         * @param value
         */
        void setAngularDamping(float value);

        /**
         * @brief Set dynamic body type (b2_dynamicBody)
         */
        void setBodyDynamic()
        {
            LOGME_CALL("RigidBody::setBodyDynamic");
            LOGME_ASSERT(nullptr == body, "Physics engine is already running");
            bodyDef.type = b2_dynamicBody;
        }

        /**
         * @brief Set kinematic body type (b2_kinematicBody)
         */
        void setBodyKinematic()
        {
            LOGME_CALL("RigidBody::setBodyKinematic");
            LOGME_ASSERT(nullptr == body, "Physics engine is already running");
            bodyDef.type = b2_kinematicBody;
        }

        /**
         * @brief Set static body type (b2_staticBody)
         */
        void setBodyStatic()
        {
            LOGME_CALL("RigidBody::setBodyStatic");
            LOGME_ASSERT(nullptr == body, "Physics engine is already running");
            bodyDef.type = b2_staticBody;
        }

        /**
         * @brief setBulletMode
         * @param enabled
         */
        void setBulletMode(bool enabled);

        /**
         * @brief Set density of the body
         *
         * From Box2D manual:
         * The fixture density is used to compute the mass properties of the parent body.
         * The density can be zero or positive. You should generally use similar densities
         * for all your fixtures. This will improve stacking stability.
         *
         * @param value
         */
        void setDensity(float value);

        /**
         * @brief Set friction parameter of the body
         *
         * From Box2D manual:
         * Friction is used to make objects slide along each other realistically. Box2D supports
         * static and dynamic friction, but uses the same parameter for both. Friction is simulated
         * accurately in Box2D and the friction strength is proportional to the normal force
         * (this is called Coulomb friction). The friction parameter is usually set between 0 and 1,
         * but can be any non-negative value. A friction value of 0 turns off friction and a
         * value of 1 makes the friction strong. When the friction force is computed between
         * two shapes, Box2D must combine the friction parameters of the two parent fixtures
         *
         *      friction = sqrtf(shape1->friction * shape2->friction);
         *
         * So if one fixture has zero friction then the contact will have zero friction.
         *
         * @param value
         */
        void setFriction(float value);

        /**
         * @brief setLinearDamping
         * @param value
         */
        void setLinearDamping(float value);

        /**
         * @brief Set restitution parameter of the body
         *
         * From Box2D manual:
         * Restitution is used to make objects bounce. The restitution value is usually set
         * to be between 0 and 1. Consider dropping a ball on a table. A value of zero means
         * the ball won't bounce. This is called an inelastic collision. A value of one means
         * the ball's velocity will be exactly reflected. This is called a perfectly elastic
         * collision. Restitution is combined using the following formula.
         *
         *      restitution = b2Max(shape1->restitution, shape2->restitution);
         *
         * @param value
         */
        void setRestitution(float value);


    private:

        /**
         * @brief setFixedRotation
         * @param enabled
         */
        void setFixedRotation(bool enabled);

        /**
         * @brief updateFixture
         */
        void updateFixture()
        {
            if (nullptr != fixture) {
                fixture->SetFilterData(fixtureDef.filter);
            }
        }

    public:
        b2Body*         body;
        b2Fixture*      fixture;
        b2World*        mWorld;
        bool            mEnabled;
        b2BodyDef       bodyDef;
        b2FixtureDef    fixtureDef;

    };
}
