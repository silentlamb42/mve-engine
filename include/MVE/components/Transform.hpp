#pragma once

#include <MVE/entity/Component.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/Vector2.hpp>
#include <iosfwd>

namespace mve
{
#ifdef MVE_LUA_ENABLED
    namespace lua
    {
        class LuaVM;
    }
#endif
}

namespace mve
{
    using Vector2f = sf::Vector2f;
    using Matrix3f = sf::Transform;

    /**
     * @brief Component describing orientation in 2D space
     *
     * TODO: Design and implement parent-child relation
     */
    class Transform : public Component<Transform>
    {
    public:
#ifdef MVE_LUA_ENABLED
        static void registerLuaContext(mve::lua::LuaVM& vm);
#endif

    public:

        Transform()
            : mTransformable {}
        {
            // NOOP
        }

        ~Transform()
        {
            // NOOP
        }

    public:

        /**
         * @brief Position of the entity (in pixels)
         *
         * @return
         */
        const Vector2f& getPosition() const
        {
            return mTransformable.getPosition();
        }

        /**
         * @brief Rotation of the entity
         *
         * @return angle in degrees
         */
        float getRotation() const
        {
            return mTransformable.getRotation();
        }

        /**
         * @brief Origin of the entity (in pixels)
         *
         * @return
         */
        const Vector2f& getOrigin() const
        {
            return mTransformable.getOrigin();
        }

        /**
         * @brief Transform Matrix
         *
         * @return
         */
        const Matrix3f& getTransform() const
        {
            return mTransformable.getTransform();
        }

        /**
         * @brief Transform inverse matrix
         *
         * @return
         */
        const Matrix3f& getInverseTransform() const
        {
            return mTransformable.getInverseTransform();
        }

        /**
         * @brief Position of the entity
         *
         * @param x in pixels
         * @param y in pixels
         */
        void setPosition(float x, float y)
        {
            mTransformable.setPosition(x, y);
        }

        /**
         * @brief Position of the entity
         *
         * @param position in pixels
         */
        void setPosition(const Vector2f& position)
        {
            mTransformable.setPosition(position);
        }

        /**
         * @brief Rotation of the entity
         *
         * @param angle in degrees
         */
        void setRotation(float angle)
        {
            mTransformable.setRotation(angle);
        }

        /**
         * @brief Set origin point of the entity
         *
         * @param x in pixels
         * @param y in pixels
         */
        void setOrigin(float x, float y)
        {
            mTransformable.setOrigin(x, y);
        }

        /**
         * @brief Set origin point of the entity
         *
         * @param origin vector in pixels
         */
        void setOrigin(const Vector2f& origin)
        {
            mTransformable.setOrigin(origin);
        }

        /**
         * @brief Move entity by an offset
         *
         * @param offsetX in pixels
         * @param offsetY in pixels
         */
        void move(float offsetX, float offsetY)
        {
            mTransformable.move(offsetX, offsetY);
        }

        /**
         * @brief Move entity by an offset
         *
         * @param offset vector in pixels
         */
        void move(const Vector2f& offset)
        {
            mTransformable.move(offset);
        }

        /**
         * @brief Rotate entity by a given angle
         *
         * @param angle in degree
         */
        void rotate(float angle)
        {
            mTransformable.rotate(angle);
        }

    private:
        sf::Transformable mTransformable;
    };

    std::ostream& operator<<(std::ostream& os, const Transform& t);

} // namespace mvcrawl
