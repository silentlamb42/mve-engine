#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/utils/Helpers.hpp>

#include <Box2D/Box2D.h>
#include <SFML/System/Vector2.hpp>


namespace mve {
namespace priv {

    /**
     * @brief The ShapeType enum
     */
    enum class ShapeType
    {
        Unknown,
        Circle,
        Polygon,
        Box
    };

    /**
     * @brief The ShapeComponent class
     */
    class ShapeComponent
    {
    public:

        ShapeComponent()
        {
            shape_type = ShapeType::Unknown;
            shape_polygon = b2PolygonShape();
            shape_circle = b2CircleShape();
        }

        ~ShapeComponent()
        {
            // NOOP
        }

        /**
         * @brief Set shape of the sensor to rectangle of size (@a w, @a h) attached to the body
         * at the center of the mass with @a offset and rotated to @a angle rad.
         *
         * @param w         width of the box in pixels
         * @param h         height of the box in pixels
         * @param offset    offset of the box in pixels (default: 0, 0)
         * @param angle     rotation angle in deg (default: 0, 0)
         */
        void setShapeBox(float w_px, float h_px, const sf::Vector2f& offset_px = sf::Vector2f(), float angle = 0.f);

        /**
         * @brief Set shape of the sensor to circle of radius @a r and attached to the body
         * at the center of the mass with @a offset (@a x, @a y).
         *
         * @param r_px       radius of the shape
         * @param offset_px  offset from the center of the mass (default: 0, 0)
         */
        void setShapeCircle(float r_px, const sf::Vector2f& offset_px = sf::Vector2f());

        /**
         * @brief setShapePolygon
         * @param points_px
         * @param count
         */
        void setShapePolygon(std::vector<sf::Vector2f> points_px, std::size_t count);

        /**
         * @brief attachShape
         * @param fixtureDef
         */
        void attachShape(b2FixtureDef& fixtureDef);

    protected:
        ShapeType       shape_type;
        b2PolygonShape  shape_polygon;
        b2CircleShape   shape_circle;
    };


} // namespace priv
} // namespace mve
