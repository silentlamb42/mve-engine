/** File: Entity.hpp
 *
 * Entity:
 * -------
 * By design it's just unsigned integer; an index to access arrays with components data.
 *
 * EntityManager:
 * --------------
 * This class holds pool of available Entities (array indices) to reuse already allocated
 * component data structures.
 *
 * The class itself does not contain any guards before dummy user.
 * Make sure create() and destroy() pair is called once, with proper arguments, so
 * release won't be called on Entity which is present in free list.
 *
 *
 * TODO: Make pool of entities grow larger if EntityMaxNum is reached (array vs vector?)
 * TODO: Make debug functionalities (like num of available/taken nodes, test for entity availability)
 * TODO: Think over making Entity a class with methods like alive()
 *
 */
#pragma once

#include <MVE/event/Events.hpp>

#include <array>
#include <stdexcept>

namespace mve
{
    // Constants
#ifndef NDEBUG
    constexpr unsigned int EntityMaxNum = 16384;
#else
    constexpr unsigned int EntityMaxNum = 8192;
#endif

    // Aliases
    using Entity = unsigned int;


    // Events
    struct MVE_EVENT(EntityCreatedEvent)
    {
        explicit EntityCreatedEvent(Entity entity) : entity(entity) {}
        virtual ~EntityCreatedEvent() {}

        Entity entity;
    };

    struct MVE_EVENT(EntityDestroyedEvent)
    {
        explicit EntityDestroyedEvent(Entity entity) : entity(entity) {}
        virtual ~EntityDestroyedEvent() {}

        Entity entity;
    };

    std::ostream& operator<<(std::ostream& os, const EntityCreatedEvent& e);
    std::ostream& operator<<(std::ostream& os, const EntityDestroyedEvent& e);


    /**
     * @brief Create and manage pool of Entities.
     *
     * In current implementation entity is just an identifier used by
     * components as a table index.
     */
    class EntityManager
    {
        /**
         * @brief Helper structure to create linked list of free Entities.
         */
        struct EntityId {
            Entity id;
            Entity next;
        };

    public:

        /**
         * @brief Default constructor
         *
         * Creates pool of @value EntityMaxNum free entities.
         */
        EntityManager(EventManager& eventManager);

        /**
         * @brief Create Entity by taking it from the object pool
         *
         * Emits EntityCreatedEvent
         *
         * @return
         */
        Entity create();

        /**
         * @brief Release entity to the pool of free entities.
         *
         * Emits EntityDestroyedEvent
         *
         * @param entity id to release
         */
        void destroy(Entity entity);

        /**
         * @brief Return Entity considered as invalid.
         *
         */
        static Entity invalid();


    private:
        EventManager&                       mEventManager;
        Entity                              mFreeNode {0};
        std::array<EntityId, EntityMaxNum>  mEntities;
    };


}
