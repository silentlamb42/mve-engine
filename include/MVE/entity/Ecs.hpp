#pragma once

#include <MVE/event/Events.hpp>
#include <memory>

// TODO: Documentation

namespace mve
{
    // Forward declarations
    template <typename T>
    class ComponentManager;

    class EntityManager;

    template <typename T>
    class SystemManager;

    /**
     *
     */
    template <typename T>
    struct EcsContext
    {
        EventManager& events;
        std::shared_ptr<EntityManager>          entity;
        std::shared_ptr<ComponentManager<T>>    component;
        std::shared_ptr<SystemManager<T>>       system;

        explicit EcsContext(EventManager& eventManager)
            : events (eventManager)
        {
            // NOOP
        }

        /**
         * @brief Returns manager of Entities
         */
        std::shared_ptr<EntityManager> e()
        {
            return entity;
        }

        /**
         * @brief Return manager of Components
         */
        std::shared_ptr<ComponentManager<T>> c()
        {
            return component;
        }

        /**
         * @brief Return manager of Systems
         */
        std::shared_ptr<SystemManager<T>> s()
        {
            return system;
        }
    };

}
