#pragma once

#include <MVE/entity/Ecs.hpp>
#include <MVE/entity/Component.hpp>
#include <MVE/entity/Entity.hpp>

#include <SFML/System/Time.hpp>

#include <map>

// TODO: Documentation
// TODO: Licence and original code notice

namespace mve
{
    using SystemType = std::size_t;
    using SystemOrder = std::size_t;
    using DeltaTime = sf::Time;

    constexpr std::size_t MaxSystemNum = 1024;

    namespace Priority {
        constexpr std::size_t FIRST = 0;
        constexpr std::size_t LAST = MaxSystemNum;
    }


    namespace priv
    {
        struct SystemKey
        {
            SystemType type;
            SystemOrder order;
        };
    }
}


namespace std
{
    template <> struct less<mve::priv::SystemKey>
    {
        bool operator() (const mve::priv::SystemKey& lhs, const mve::priv::SystemKey& rhs)
        {
            return (lhs.order < rhs.order)
                    || (lhs.order == rhs.order && lhs.type < rhs.type);
        }
    };
}


namespace mve
{
    template <typename T>
    class SystemManager;

    class EngineContext;

    /**
     * @brief The BaseSystem class
     */
    template <typename T>
    class BaseSystem
    {
    public:
        virtual ~BaseSystem() {}

        /**
         * @brief configure
         */
        virtual void configure(EngineContext& engine, EcsContext<T>& context) = 0;

        /**
         * @brief update
         * @param dt
         */
        virtual void update(EcsContext<T>& context, DeltaTime dt) = 0;

        /**
         * @brief Return number of known systems
         */
        static SystemType count()
        {
            return sTypeCounter;
        }

    protected:
        static SystemType sTypeCounter;
    };

    template <typename T>
    SystemType BaseSystem<T>::sTypeCounter = 0;

    /**
     *
     */
    template <typename Derived, typename ComponentData, std::size_t Priority = 5>
    class System : public BaseSystem<ComponentData>
    {
    public:
        using BaseSystem<ComponentData>::sTypeCounter;

    public:
        virtual ~System() {}

        /**
         * @brief Return Id of this System
         *
         * For new subclasses the first time this method is called, the new unique ID
         * is assigned just for this subclass.
         *
         * @return
         */
        static SystemType type()
        {
            static SystemType type = sTypeCounter++;
            return type;
        }

    private:

        /**
         * @brief Return key indicating when should this system be processed.
         * @return
         */
        constexpr static priv::SystemKey key()
        {
            return priv::SystemKey {type(), Priority};
        }

        friend class SystemManager<ComponentData>;
    };

    /**
     * @brief The SystemManager class
     */
    template <typename ComponentData>
    class SystemManager
    {
    public:

        explicit SystemManager(EngineContext& engine, EcsContext<ComponentData>& context)
            : mEngineContext(engine)
            , mEcsContext(context)
        {

        }

        /**
         *
         */
        template <typename S, typename... Args>
        std::shared_ptr<S> add(Args&& ... args)
        {
            LOGME_CALL("SystemManager::add");
            LOGME_INFO("Registering: %s", S::name());
            std::shared_ptr<S> ptr(new S(std::forward(args)...));
            mSystems.insert(std::make_pair(S::key(), ptr));
            return ptr;
        }

        /**
         *
         */
        template <typename S>
        std::shared_ptr<S> get()
        {
            LOGME_CALL_SILENT("SystemManager::get");
            auto found = mSystems.find(S::key());
            LOGME_ASSERT(found != mSystems.end(), "System not registered");

            // static_pointer_cast: static_cast for shared_ptr
            return std::shared_ptr<S>(std::static_pointer_cast<S>(found->second));
        }

        /**
         * @brief Configure each registered system.
         */
        void configure()
        {
            for (auto& pair : mSystems)
            {
                pair.second->configure(mEngineContext, mEcsContext);
            }
        }

        /**
         * @brief Update internal ticks
         * @param dt
         */
        void update(DeltaTime dt)
        {
            // Update systems from lowest to highest priority number
            for (auto it = mSystems.begin(); it != mSystems.end(); ++it) {
                it->second->update(mEcsContext, dt);
            }
        }

    private:

        using MapKey = priv::SystemKey;
        using MapValue = std::shared_ptr<BaseSystem<ComponentData>>;

        EngineContext&              mEngineContext;
        EcsContext<ComponentData>&  mEcsContext;
        std::map<MapKey, MapValue>  mSystems;
    };
}

