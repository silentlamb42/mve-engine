#pragma once

#include <MVE/entity/Entity.hpp>
#include <MVE/event/Events.hpp>
#include <MVE/utils/Helpers.hpp>
#include <MVE/utils/Logger.hpp>

#include <array>
#include <bitset>
#include <tuple>
#include <memory>
#include <vector>
#include <cstring>

// TODO: Documentation

/**
 * Macro to hide verbose template parameters when declaring a System.
 *
 * Used to prevent creation of invalid Components due to copy-pasting, like:
 *
 *     class MyComponent : public mve::Component<SomeComponent>
 *
 * TODO: Remove it as it makes QtCreator's highlighter and syntax helper not working properly.
 */
#ifndef MVE_COMPONENT
    #define MVE_COMPONENT(NAME) NAME : public mve::Component<NAME>
#endif


#define MVE_MEM_DEBUG_AVAIL_SYMBOL      0xFE
#define MVE_MEM_DEBUG_TAKEN_SYMBOL      0xFC
#define MVE_MEM_DEBUG_RELEASED_SYMBOL   0xF8


// TODO: Licence

namespace mve
{
    constexpr std::size_t MaxComponentNum   = MVE_MAX_COMPONENTS;
    constexpr std::size_t EntitiesPerBlock  = MVE_ENTITIES_PER_BLOCK;

    using ComponentMask = std::bitset<MaxComponentNum>;

    /**
     * @brief Base class for Components used to keep counter of Component types.
     *
     * Every derived class will increase the counter and receive unique ID this way.
     */
    class BaseComponent
    {
    public:
        using Type = std::size_t;

    public:
        static Type count();

    protected:
        static Type sTypeCounter;
    };

    /**
     *
     */
    template <typename Derived>
    struct Component : public BaseComponent
    {
        static Type type()
        {
            LOGME_CALL_SILENT("Component::type");
            static Type type = sTypeCounter++;
            LOGME_ASSERT(count() <= MaxComponentNum, "Too many registered components.");
            return type;
        }
    };

    /**
     *
     */
    struct MVE_EVENT(ComponentAttachedEvent)
    {
        explicit ComponentAttachedEvent(Entity entity, std::size_t component)
            : entity (entity), component (component) {}
        virtual ~ComponentAttachedEvent() {}

        Entity entity;
        std::size_t component;
    };

    /**
     *
     */
    struct MVE_EVENT(ComponentDetachedEvent)
    {
        explicit ComponentDetachedEvent(Entity entity, std::size_t component)
            : entity (entity), component (component) {}
        virtual ~ComponentDetachedEvent() {}

        Entity entity;
        std::size_t component;
    };

    std::ostream& operator<<(std::ostream& os, const ComponentAttachedEvent& e);
    std::ostream& operator<<(std::ostream& os, const ComponentDetachedEvent& e);

    namespace priv
    {
        /**
         * @brief Class to group together component data and component mask for an entity
         *
         * Each entity got its own instance.
         */
        template <typename T>
        struct DataContainer
        {
            bool   allocated{false};
            char   container[sizeof(T)];

            DataContainer()
            {
#ifndef MVE_ECS_DEBUG_MEM
                std::memset(container, MVE_MEM_DEBUG_AVAIL_SYMBOL, sizeof(T));
#endif
            }

            T& data()
            {
                return *(reinterpret_cast<T*>(container));
            }

            ComponentMask& enabled()
            {
                return data().getMask();
            }
        };


        template <typename T>
        using DataBlock = std::array<DataContainer<T>, EntitiesPerBlock>;

        template <typename T>
        using DataBlockPtr = std::unique_ptr<DataBlock<T>>;

        /**
         * Set bit in ComponentMask related to the given component
         */
        template <typename C>
        inline void fillMask(ComponentMask& mask)
        {
            mask.set(C::type());
        }

        /**
         * Set bits in ComponentMask for each given Component
         */
        template <typename C0, typename C1, typename... CN>
        inline void fillMask(ComponentMask& mask)
        {
            fillMask<C0>(mask);
            fillMask<C1, CN...>(mask);
        }

        /**
         * @brief Get Component Mask for given Component classes.
         *
         * Note: Use alphabetical order when passing template parameters.
         */
        template <typename... C>
        inline ComponentMask getMask()
        {
            ComponentMask mask;
            fillMask<C...>(mask);
            return mask;
        }

        /**
         * @brief Determine whether @a src mask got all required bits set given as @a test mask.
         *
         * @param src   source mask to validate
         * @param test  list of components to check
         * @return true if mask @a src matches @a test mask
         */
        inline bool hasAllComponent(const ComponentMask& src, const ComponentMask test)
        {
            return (src & test) == test;
        }

        /**
         * @brief Determine whether @a src mask got enabled given component
         */
        template <typename C>
        inline bool hasComponent(const ComponentMask& src)
        {
            return src.test(C::type());
        }
    }

    /**
     * @brief Manage component related data.
     *
     * ComponentManager class provides methods to:
     *
     * - enable/disable selected components for given entity.
     * - retrieve contaier of components
     * - process entities having selected component enabled
     *
     */
    template <typename T>
    class ComponentManager
            : public Receiver<EntityCreatedEvent>
            , public Receiver<EntityDestroyedEvent>
    {
    public:
        using ProcessCallback = std::function<void(Entity, T&)>;


    public:

        ComponentManager(EventManager& eventManager)
            : mEventManager(eventManager)
        {
            // Allocate single block at start
            typename priv::DataBlockPtr<T> ptr(new priv::DataBlock<T>);
            mDataBlocks.push_back(std::move(ptr));

            // Subscribe to events to call ctors and dtors
            mEventManager.subscribe<EntityCreatedEvent>(*this);
            mEventManager.subscribe<EntityDestroyedEvent>(*this);
        }

    public:

        /**
         *
         */
        template <typename... C>
        T& attach(Entity entity, bool enabled = true)
        {
            LOGME_CALL_SILENT("ComponentManager::enable");
            auto& c = container(entity);

            ComponentMask mask = priv::getMask<C...>();
            std::size_t componentBit = 0;

            // Analyze first bit then shift whole mask, as long as mask is not empty
            while (mask.any())
            {
                if (mask.test(0))
                {
                    LOGME_TRACE("Changing component id: %d to: %s", componentBit, enabled ? "enabled" : "disabled");
                    if (enabled)
                    {
                        mEventManager.send<ComponentAttachedEvent>(entity, componentBit);
                    }
                    else
                    {
                        mEventManager.send<ComponentDetachedEvent>(entity, componentBit);
                    }

                    // TODO: friend class?
                    c.enabled().set(componentBit, enabled);
                }

                ++componentBit;
                mask = mask >> 1;
            }
            return c.data();
        }


    private:
        priv::DataContainer<T>& container(Entity entity)
        {
            LOGME_CALL_SILENT("ComponentManager::container");
            std::size_t blockNum = entity / EntitiesPerBlock;
            Entity pos = entity % EntitiesPerBlock;

            // TODO: OoM
            priv::DataBlock<T>& block = *mDataBlocks[blockNum];
            return block[pos];
        }

    public:

        /**
         * @brief get
         * @param entity
         * @return
         */
        T& get(Entity entity)
        {
            LOGME_CALL_SILENT("ComponentManager::get");
            priv::DataContainer<T>& c = container(entity);
            LOGME_ASSERT(c.allocated == true, "Entity not created yet (allocated: false!)");
            return c.data();
        }

        /**
         * @brief Method to check if entity, given as component container, has attached
         *        selected Container.
         */
        template <typename C>
        inline bool contains(T& data)
        {
            return data.getMask().test(C::type());
        }

        /**
         *
         */
        template <typename... C>
        void process(ProcessCallback callback)
        {
            LOGME_CALL_SILENT("ComponentManager::process");
            ComponentMask mask = priv::getMask<C...>();

            for (std::size_t blockNum = 0; blockNum < mDataBlocks.size(); ++blockNum)
            {
                priv::DataBlock<T>& block = *mDataBlocks[blockNum];
                for (std::size_t pos = 0; pos < block.size(); ++pos)
                {
                    priv::DataContainer<T>& data = block[pos];
                    if (data.allocated && priv::hasAllComponent(data.enabled(), mask))
                    {
                        // callback(entity, T&)
                        callback(blockNum * EntitiesPerBlock + pos, data.data());
                    }
                }
            }
        }

    public:

        /**
         * @brief receive
         * @param event
         */
        void receive(const EntityCreatedEvent& event)
        {
            LOGME_CALL_SILENT("ComponentManager::receive(EntityCreatedEvent)");
//            LOGME_INFO("Entity created event: %d", event.entity);
            auto& c = container(event.entity);

            if (c.allocated)
            {
                LOGME_ERROR("Memory for entity %d already allocated!");
                throw std::bad_alloc();
            }

            void* ptr = reinterpret_cast<void*>(c.container);

#ifndef MVE_ECS_DEBUG_MEM
                std::memset(ptr, MVE_MEM_DEBUG_TAKEN_SYMBOL, sizeof(T));
#endif

            // TODO: Make sure the size is ok
            // Call ctor using placement new
            new (reinterpret_cast<void*>(ptr)) T;

            c.allocated = true;
        }

        /**
         * @brief receive
         * @param event
         */
        void receive(const EntityDestroyedEvent& event)
        {
            LOGME_CALL_SILENT("ComponentManager::receive(EntityDestroyedEvent)");
            auto& c = container(event.entity);
            c.enabled().reset();

            LOGME_ASSERT(c.allocated, "Memory has not been acquired yet");

            // Call explicit dtor
            T* ptr = &(c.data());
            ptr->~T();
#ifndef MVE_ECS_DEBUG_MEM
                std::memset(&c, MVE_MEM_DEBUG_RELEASED_SYMBOL, sizeof(T));
#endif
            c.allocated = false;
        }


    private:
        EventManager& mEventManager;
        std::vector<  std::unique_ptr< priv::DataBlock<T> >  > mDataBlocks;
    };


}
