#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/res/Resource.hpp>
#include <MVE/utils/Logger.hpp>

#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

// TODO: Documentation

namespace mve
{
    /**
     * @brief Class to organize resources in a single place. It allows to assign identiefiers
     *        to resources,
     */
    class ResourceManager
    {
        struct Loader
        {
            std::function<void(void)>   loader;
            long                        hash;
        };

        std::unordered_map<MvResId, Resource<sf::Font>>        mFontResources;
        std::unordered_map<MvResId, Resource<sf::Shader>>      mShaderResources;
        std::unordered_map<MvResId, Resource<sf::SoundBuffer>> mSoundBufferResources;
        std::unordered_map<MvResId, Resource<sf::Texture>>     mTextureResources;

        std::unordered_map<MvResId, Loader>                    mResourceLoaders;


    public:

        ResourceManager()
        {
            // NOOP
        }

    public:

        /**
         * @brief registerFont
         * @param id
         * @param filename
         */
        void registerFont(MvResId id, const std::string &filename);

        /**
         * @brief registerShader
         * @param id
         * @param vert
         * @param frag
         */
        void registerShader(MvResId id, const std::string &vert, const std::string &frag);

        /**
         * @brief registerSoundBuffer
         * @param id
         * @param filename
         */
        void registerSoundBuffer(MvResId id, const std::string &filename);

        /**
         * @brief registerTexture
         * @param id
         * @param filename
         */
        void registerTexture(MvResId id, const std::string &filename);

    public:

        /**
         * @brief loadFont
         * @param id
         */
        void load(MvResId id);

        /**
         * @brief unloadFont
         * @param id
         */
        void unload(MvResId id);

    public:

        /**
         * @brief getFont
         * @param id
         * @return
         */
        sf::Font& getFont(MvResId id);

        /**
         * @brief getShader
         * @param id
         * @return
         */
        sf::Shader& getShader(MvResId id);

        /**
         * @brief getSoundBuffer
         * @param id
         * @return
         */
        sf::SoundBuffer& getSoundBuffer(MvResId id);

        /**
         * @brief getTexture
         * @param id
         * @return
         */
        sf::Texture& getTexture(MvResId id);

    private:

//        void loadFont(MvResId id,
    };
}
