#pragma once

#include <MVE/core/Types.hpp>

#include <memory>
#include <stdexcept>

namespace mve
{
    /**
     * @brief Resource holder class
     *
     * Resource under the hood is created when this object is created. It's kept in unitialized
     * state in a shared pointer. Clients from now on should call load(...) method to load the
     * resource from a file. Only first call loads the resource, the other calls only increase
     * reference counter. If you pass different arguments they will be ignored. This class
     * won't be used directly so it shouldn't be major issue.
     *
     * Notes/TODO:
     * - Later on some strategy pattern or class hierarchy could be added to have both file
     *   resources and in-memory resources.
     * - load(Args&&...) is ignored after the first call. This should be designed different way.
     */
    template <typename T>
    class Resource
    {
        std::shared_ptr<T>  mResource;
        unsigned int        mRefCount {0};

    public:

        /**
         * @brief Constructs uninitialized resource of type <T>
         */
        Resource()
            : mResource(new T)
        {
            // NOOP
        }

    public:

        /**
         * @brief Get instance of loaded resource.
         *
         * @return Reference to instance of the resource of type <T>
         */
        T& get()
        {
            if (mRefCount == 0) {
                throw std::runtime_error("Resource needs to be loaded first");
            }
            return *mResource;
        }

        /**
         * @brief Load resource if not loaded yet
         *
         * The method increases internal ref counter. If the counter is 1, the resource is loaded.
         */
        template <typename... Args>
        void load(Args&&... args)
        {
            if (mRefCount++ == 0) {
                if (!mResource->loadFromFile(std::forward<Args>(args)...))
                {
                    throw std::runtime_error("Failed to load resource");
                }
            }
        }

        /**
         * @brief Release resource if there is no more clients using it
         *
         * The method decreases internal ref counter. If the counter is 0, the resource is released.
         */
        void unload()
        {
            if (mRefCount-- > 0)
            {
                mResource.reset();
            }
        }
    };
}
