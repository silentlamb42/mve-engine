#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/utils/Logger.hpp>
#include <SFML/System/Time.hpp>

#include <vector>
#include <memory>

// TODO: Documentation

namespace sf
{
    class Event;
}

namespace mve
{
    class Director;
    class EngineContext;
}

namespace mve
{
    /**
     * @brief
     */
    class Scene
    {
    public:
        using Ptr = std::unique_ptr<Scene>;

    public:

        Scene(Director& director, EngineContext& context);
        virtual ~Scene();

    public:

        /**
         * @brief Handle SFML event
         *
         * @return flag indicating whether event handling should be continued down the stack
         */
        virtual bool handleEvent(const sf::Event& event) = 0;

        /**
         * @brief Prepare Scene before the first tick
         */
        virtual void initialize();

        /**
         * @brief Draw content of the Scene to the screen.
         * @see Director#render()
         *
         */
        virtual void render() = 0;

        /**
         * @brief update Update logic of the Scene

         *
         * @param dt Time before previous update
         * @see Director#update(sf::Time)
         *
         * @return flag indicating whether
         */
        virtual bool update(sf::Time dt) = 0;

    protected:

        EngineContext& getContext()
        {
            return mContext;
        }

        Director& getDirector()
        {
            return mDirector;
        }

    private:
        EngineContext&  mContext;
        Director&       mDirector;
    };

}
