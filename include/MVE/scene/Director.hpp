#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/utils/Logger.hpp>
#include <MVE/scene/Scene.hpp>

#include <memory>
#include <vector>
#include <unordered_map>
#include <queue>

// TODO: Documentation

namespace sf
{
    class Event;
}

namespace mve
{
    class Scene;
    class EngineContext;
}

namespace mve
{
    using FactoryFunc = std::function<Scene::Ptr(void)>;

    /**
     * @brief The TransitionType enum
     */
    enum class TransitionType
    {
        None,
    };

    /**
     * @brief DEFAULT_TRANSITION
     */
    constexpr TransitionType DEFAULT_TRANSITION = TransitionType::None;

    /**
     * @brief The Director class
     */
    class Director
    {
    public:
        using Ptr = std::unique_ptr<Director>;

    public:

        Director(EngineContext& context);

        Director(const Director& director) = delete;
        Director& operator=(const Director& director) = delete;

        Director(Director&& director) = default;
        Director& operator=(Director&& director) = default;

        virtual ~Director();

    public:

        /**
         * @brief Pass event to the Scenes from top to bottom
         *
         * If any Scene handlers returns with false, event handling is stopped.
         *
         * @param event
         */
        void handleEvent(const sf::Event& event);

        /**
         * @brief Render Scenes from bottom to top
         *
         * Scenes are drawn from bottom to top for transparency reasons.
         * It might be wise not to keep unnecessary Scenes on the stack.
         *
         */
        void render();

        /**
         * @brief Update logic of all Scenes on the stack
         *
         * Scenes are updated from top to bottom. If any Scene update() call returns false,
         * The whole process is stopped, so lower layers won't be updated.
         *
         * This might be used to make lower layers inactive for any update, but make them
         * still render on the screen. Good example is a Pause Scene.
         *
         * @param dt
         */
        void update(sf::Time dt);

    public:

        /**
         * @brief hasScene
         * @return
         */
        bool hasScene() const
        {
            return mStack.size() > 0;
        }

        /**
         * @brief registerScene
         * @param id
         * @param func
         */
        void registerScene(MvSceneId id, FactoryFunc func);

        /**
         * @brief Remove all Scenes from the stack
         */
        void sceneClear();

        /**
         * @brief Remove Scene from the top of the stack.
         *
         * Use transition for a state being removed.
         *
         * @param transition
         */
        void scenePop(TransitionType transition = DEFAULT_TRANSITION);

        /**
         * @brief Add scene to the top of the stack with given transition.
         *
         * @param sceneId
         * @param transition
         */
        void scenePush(MvSceneId sceneId, TransitionType transition = DEFAULT_TRANSITION);

        /**
         * @brief Add Scene at the end of the queue.
         *
         * Scenes on the stack are seen imediately. Scenes in the queue waits there until
         * there's no Scene on the stack. This allows to have transitions like
         *
         *      brand movie -> intro movie -> main menu
         *
         * All these scenes might call just scenePop and transition will be done automatically
         * without them knowing which scene is next.
         *
         * @param sceneId       Id of the Scene to queue
         * @param transition    Transition to use when scene is about to be seen
         */
        void sceneQueue(MvSceneId sceneId, TransitionType transition = DEFAULT_TRANSITION);

        /**
         * @brief Remove Scene on the top of the stack, push new Scene on the stack.
         *
         * The effect is similar two these two sequential requests:
         * - scenePop
         * - scenePush(sceneId)
         *
         * @param sceneId       Id of the Scene to switch to
         * @param transition    Transition to use when switching scenes
         */
        void sceneSwitch(MvSceneId sceneId, TransitionType transition = DEFAULT_TRANSITION);


    private:

        Scene::Ptr createScene(MvSceneId id);

        /**
         * @brief Handle scene requests
         */
        void refresh();

        /**
         * @brief The RequestType enum
         */
        enum class RequestType
        {
            Clear,
            Pop,
            Push,
            Queue,
            Switch,
        };

        /**
         * @brief The Request struct
         */
        struct Request
        {
            const RequestType     type;
            const MvStateId       stateId;
            const TransitionType  transition;

            explicit Request(RequestType type)
                : type(type)
                , stateId()
                , transition(DEFAULT_TRANSITION)
            {
                // NOOP
            }

            explicit Request(RequestType type, TransitionType transition)
                : type(type)
                , stateId()
                , transition(transition)
            {
                // NOOP
            }

            explicit Request(RequestType type, MvId stateId, TransitionType transition)
                : type(type)
                , stateId(stateId)
                , transition(transition)
            {
                // NOOP
            }
        };

        using HandlerFunc = std::function<void(const Request&)>;


    private:
        EngineContext& mContext;
        std::vector<Scene::Ptr> mStack;
        std::queue<Scene::Ptr> mQueue;
        std::vector<Request>    mRequests;
        std::unordered_map<MvUint,    HandlerFunc> mHandlers;
        std::unordered_map<MvStateId, FactoryFunc> mFactory;
    };
}

