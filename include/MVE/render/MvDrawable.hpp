#pragma once

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/Time.hpp>
#include <memory>

// TODO: Documentation

namespace sf
{
    class RenderTarget;
}

namespace mve
{
#ifdef MVE_LUA_ENABLED
    namespace lua
    {
        class LuaVM;
    }
#endif

    class MvDrawable : public sf::Transformable
    {
    public:
        using Ptr = std::unique_ptr<MvDrawable>;

    public:
#ifdef MVE_LUA_ENABLED
        static void initDataTable(lua::LuaVM& vm);
#endif

    public:
        virtual ~MvDrawable();

    public:

        /**
         * @brief setPosition
         * @param x
         * @param y
         */
        void setPosition(float x, float y);

        /**
         * @brief setPosition
         * @param position
         */
        void setPosition(const sf::Vector2f& position);

        /**
         * @brief draw
         * @param target
         * @param state
         */
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states);

        /**
         * @brief update
         * @param dt
         */
        virtual void update(sf::Time dt);

        /**
         * @brief onPositionChanged
         * @param pos
         */
        virtual void onPositionChanged(const sf::Vector2f& pos);

        /**
         * @brief flipX
         */
        virtual void flipX();

        /**
         * @brief flipY
         */
        virtual void flipY();
    };
}
