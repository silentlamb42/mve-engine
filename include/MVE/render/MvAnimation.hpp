#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/render/MvDrawable.hpp>
#include <SFML/System/Time.hpp>

#include <vector>
#include <unordered_map>

namespace mve
{
    /**
     * @brief Type of repeat capability of animation
     */
    enum class RepeatMode {
        Once,   /*< Play once and stop */
        Loop,   /*< Play infinitely (start at beginning) */

        // TODO: Loop_Reverse
//        Loop_Reverse,   /*< Play infinitely (reverse when loop is done) */
    };

    struct FrameSpec
    {
        MvId        frame;
        sf::Time    endPos;

        FrameSpec() : frame {0}, endPos {sf::Time::Zero} {}
        FrameSpec(MvId frame, sf::Time endPos) : frame {frame}, endPos {endPos} {}
    };

#ifdef MVE_LUA_ENABLED
    namespace lua {
        class LuaVM;
    }
#endif

    // Class def

    class MvAnimation : public MvDrawable
    {
    public:
        using Ptr = std::unique_ptr<MvAnimation>;

    public:
#ifdef MVE_LUA_ENABLED
        static void registerLuaContext(mve::lua::LuaVM& vm);
#endif

    public:
        /**
         * @brief Construct animation drawable of variadic frame duration
         */
        MvAnimation(RepeatMode mode = RepeatMode::Loop);

    public:

        /**
         * @brief Adds a frame to the animation.
         *
         * Frame is Id of previously added drawable.
         *
         * @param id        id of the drawable
         * @param duration  frame length
         */
        void addFrame(MvId drawableId, sf::Time duration);

        /**
         * @brief Add drawable to the animation, so it can be used to add animation frames.
         *
         * Note: Drawables are given internal ID starting from 0. Client needs to remember
         * order of drawables, and use this order in #addFrame(MvId, sf::Time) method call.
         *
         * @param drawable  drawable to be rendered
         */
        void addDrawable(MvDrawable::Ptr drawable);

    public:
        void draw(sf::RenderTarget &target, sf::RenderStates state) override;
        void flipX() override;
        void update(sf::Time dt) override;
        void onPositionChanged(const sf::Vector2f &pos) override;


    private:
        sf::Time                            mAnimationLength {sf::Time::Zero};
        sf::Time                            mCurFrameTimer {sf::Time::Zero};
        std::size_t                         mCurFrameIndex {0};
        bool                                mFinished {false};
        RepeatMode                          mRepeatMode;
        std::vector<MvDrawable::Ptr>        mDrawables {};
        std::vector<FrameSpec>              mFrames {};
    };
}
