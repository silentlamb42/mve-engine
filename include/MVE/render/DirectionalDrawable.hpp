#pragma once

#include <MVE/render/StateDrawable.hpp>
#include <MVE/core/Types.hpp>
#include <MVE/utils/Math.hpp>

// TODO: Documentation

namespace mve
{
    // Note: Needs to be power of 2
    constexpr MvUint DEFAULT_DIRECTIONS_NUM = 8;

    /**
     * @brief The DirectionalDrawable class
     */
    class DirectionalDrawable : public StateDrawable
    {
    public:
        using Ptr = std::unique_ptr<DirectionalDrawable>;

    public:

        /**
         * @brief Set number of directions for a single state.
         *
         * @param num
         */
        void setDirectionsNum(MvUint num)
        {
            mDirectionsNum = math::next_pow2(num);
        }

        /**
         * @brief setDirection
         * @param direction
         */
        void setDirection(MvDirection direction)
        {
            LOGME_CALL_SILENT("DirectionalDrawable::setDirection");
            LOGME_ASSERT(direction < mDirectionsNum, "Direction id exceeds max allowed value",
                         direction, mDirectionsNum);
#if 0
            LOGME_DEBUG("Changing direction: %d -> %d", mDirection, direction);
#endif
            mDirection = direction;
        }

        /**
         * @brief getDirection
         * @return
         */
        MvDirection getDirection() const
        {
            return mDirection;
        }

        /**
         * @brief setDrawable
         * @param state
         * @param direction
         * @param drawable
         */
        void setDrawable(MvStateId state, MvDirection direction, MvDrawable::Ptr drawable);

    public:

        void draw(sf::RenderTarget &target, sf::RenderStates states) override;
        void update(sf::Time dt) override;

    protected:

        MvStateId getDirectionalState(MvStateId state, MvDirection direction) const
        {
            return state * mDirectionsNum + direction;
        }

    private:
        MvDirection mDirection;
        MvUint mDirectionsNum {DEFAULT_DIRECTIONS_NUM};
    };

}
