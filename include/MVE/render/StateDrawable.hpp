#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/render/MvDrawable.hpp>
#include <MVE/utils/Logger.hpp>

#include <assert.h>
#include <functional>
#include <memory>
#include <unordered_map>
#include <utility>

// TODO: Documentation

namespace mve
{

    constexpr MvStateId DEFAULT_STATE_ID = 0;

    /**
     *
     */
    class StateDrawable : public MvDrawable
    {
    public:
        using Ptr = std::unique_ptr<StateDrawable>;

        /**
         * @brief Return reference to Drawable bound to the given state.
         *
         * @param state
         * @return
         */
        MvDrawable& getDrawable(MvStateId state)
        {
            LOGME_CALL_SILENT("StateDrawable::getDrawable");
            auto found = mDrawableStates.find(state);
            LOGME_ASSERT(found != mDrawableStates.end(), "No drawable bound to the state (key:%d)", state);
            return *(found->second);
        }

        MvStateId getState() const
        {
            return mCurrentMvStateId;
        }

        /**
         * @brief setDrawable
         * @param state
         * @param drawable
         */
        void setDrawable(MvStateId state, MvDrawable::Ptr drawable)
        {
            mDrawableStates[state] = std::move(drawable);
        }

        /**
         * @brief Update internal state
         *
         * @param state
         */
        void setState(MvStateId state);

    public:

        /**
         * @brief Render Drawable bound to current state.
         *
         * @param target    Rendering target (window, texture)
         * @param states    Rendering context
         */
        void draw(sf::RenderTarget& target, sf::RenderStates states)
        {
            getDrawable(mCurrentMvStateId).draw(target, states);
        }

        /**
         * @brief Update Drawable bound to current state.
         *
         * @param dt    time passed since last update
         */
        void update(sf::Time dt)
        {
            getDrawable(mCurrentMvStateId).update(dt);
        }

        /**
         * @brief Called when setPosition() of a base class has been called.
         *
         * @param pos   new position
         */
        void onPositionChanged(const sf::Vector2f &pos) override;

        /**
         * @brief flipX
         */
        void flipX() override;

    protected:

        /**
         * @brief getCurrentStateId
         * @return
         */
        MvStateId getCurrentStateId() const
        {
            return mCurrentMvStateId;
        }

        /**
         * @brief setCurrentStateId
         * @param id
         */
        void setCurrentStateId(MvStateId id)
        {
            mCurrentMvStateId = id;
        }

    private:
        std::unordered_map<MvStateId, MvDrawable::Ptr> mDrawableStates;
        MvStateId mCurrentMvStateId {DEFAULT_STATE_ID};
    };
}
