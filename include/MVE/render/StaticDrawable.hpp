#pragma once

#include <MVE/core/Types.hpp>
#include <MVE/core/EngineContext.hpp>
#include <MVE/render/MvDrawable.hpp>

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <memory>

// TODO: Documentation

namespace mve
{
    /**
     * @brief The StaticDrawable class
     */
    class StaticDrawable : public MvDrawable
    {
    public:
        using Ptr = std::unique_ptr<StaticDrawable>;

    public:
        StaticDrawable(EngineContext& context, MvId texId, sf::IntRect texRectangle);

    public:
        void draw(sf::RenderTarget &target, sf::RenderStates states) override;
        void flipX() override;
        void onPositionChanged(const sf::Vector2f &pos) override;
        void update(sf::Time dt) override;

    private:
       sf::Sprite   mSprite;
       bool         mFlipped {false};
    };
}
