#pragma once

#include <sstream>

#include <Box2D/Box2D.h>
#include <SFML/System/Vector2.hpp>

// TODO: Cleanup the file

namespace mve
{
    namespace utils
    {
        template <typename T>
        inline std::string toString(const T& value)
        {
            std::stringstream stream;
            stream << value;
            return stream.str();
        }

        inline std::string toString(const bool& value)
        {
            return value ? "enabled" : "disabled";
        }

        template <class P, class M>
        std::size_t offset_of (const M P::*member)
        {
            return (std::size_t) &(reinterpret_cast<P*>(0)->*member);
        }

        template <class P, class M>
        P* container_of(M* ptr, const M P::*member)
        {
            return reinterpret_cast<P*>(reinterpret_cast<char*>(ptr) - offset_of(member));
        }
    }

    // TODO: Create Units.hpp file
    // TODO: Move calls to units::*
    constexpr float MeterInPixels = 16;

    constexpr float pixToMetric(float pixels) {
        return pixels / MeterInPixels;
    }

    constexpr float metricToPix(float meters) {
        return meters * MeterInPixels;
    }

    namespace units {

        constexpr float pixToMetric(float pixels) {
            return pixels / MeterInPixels;
        }

        constexpr float metricToPix(float meters) {
            return meters * MeterInPixels;
        }

        inline sf::Vector2f metricToPix(const b2Vec2& meters)
        {
            return sf::Vector2f(metricToPix(meters.x), metricToPix(meters.y));
        }

        inline b2Vec2 pixToMetric(const sf::Vector2f& pixels)
        {
            return b2Vec2(pixToMetric(pixels.x), pixToMetric(pixels.y));
        }

    }
}
