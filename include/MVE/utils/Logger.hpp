/*
Flags:
    LOGME_DISABLED              Disable logger in both debug/release modes.
    LOGME_RELEASE_ENABLED       Enable logger in release mode (disabled by default)
    LOGME_USE_LOCKS             Use sf::Lock
    LOGME_256_COLORS            Dump logs with 256 color codes
    LOGME_DEFAULT_LEVEL=Level   Set default log level (NONE, TRACE, DEBUG, INFO, WARN, ERROR)

*/
#pragma once

// TODO: Documentation
// TODO: Cleanup the licence

#include <SFML/System/Clock.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Mutex.hpp>
#include <SFML/System/Lock.hpp>

#include <iostream>
#include <stdexcept>
#include <iomanip>


#if !defined(LOGME_DISABLED) && defined(NDEBUG) && !defined(LOGME_RELEASE_ENABLED)
    #define LOGME_DISABLED
#endif

#ifdef LOGME_USE_LOCKS
    #define LOGME_LOCK   sf::Lock lock(sMutex)
#else
    #define LOGME_LOCK   do {} while(0)
#endif

#ifndef LOGME_DEFAULT_LEVEL
    #ifndef NDEBUG
        #define LOGME_DEFAULT_LEVEL DEBUG
    #else
        #define LOGME_DEFAULT_LEVEL INFO
    #endif
#endif


namespace mve {

    /* Exit from template recursion */
    template <typename... Args>
    void printf(std::ostream& os, const char* s)
    {
        while (*s) {
            if (*s == '%' && *(++s) != '%') {
                throw std::runtime_error("Invalid format string: missing arguments");
            }
            os << *s++;
        }
    }

    /* Details: http://en.wikipedia.org/wiki/Variadic_template */
    template <typename T, typename... Args>
    void printf(std::ostream& os, const char* s, T&& value, Args&&... args)
    {
        while (*s) {
            if (*s == '%' && *(++s) != '%') {
                os << value;
                ++s;
                printf(os, s, args...);
                return;
            }
            os << *s++;
        }
        throw std::runtime_error("Extra arguments provided to Logger");
    }

// TODO: Remove TRACE level

/**
 * Logger based on RAII and macros
 */
class Logger
{
public:

    enum Level {
        NONE,
        TRACE,
        DEBUG,
        INFO,
        WARN,
        ERROR,
    };


    Logger(const char* owner, bool silent);
    ~Logger();


    /**
     * Display log message. Before this method call one needs to
     * create instance of Logger first. Call LOG_START or LOG_SILENT first.
     */
    template<typename... Args>
    void debug(Level level, const int line, const char* file, const char* msg, Args&&... args) const
    {
#ifndef LOGME_DISABLED
#ifdef LOGME_USE_LOCKS
        sf::Lock lock(sMutex);
#endif

        // Test current level of verbosity
        if (!checkVerbosity(level)) {
            return;
        }

        if (sUseAnsiColorSequences) {
            *sStream << "\033[" << logLevelToColor(level) << "m";
        }

        logHeader(level);
        *sStream << " " << std::left << std::setw(32) << mOwner << "  ";

        // TODO: std::forward?
        printf(*sStream, msg, args...);

        if (sLogSourcePosition) {
            *sStream << "\t[" << file << ":"
                << line << "]";
        }

        if (sUseAnsiColorSequences) {
            *sStream << "\033[0m";
        }

        *sStream << std::endl;
#endif
    }


public:
    /**
     * Enable/disable logging function calls/returns
     */
    static void setLogFunctionCalls(bool enabled);

    /**
     * Enable/disable logging __FILE__ and __LINE__ values
     */
    static void setLogSourcePosition(bool enabled);

    /**
     * Set default debug level. Log calls below this level will not be shown.
     */
    static void setDebugLevel(int level);

    /**
     * @brief setAnsiColorSequences
     * @param enabled
     */
    static void setAnsiColorSequences(bool enabled);

    /**
     * Restart time value shown in logs.
     */
    static void restartClock();

    /**
     * Set output stream of logger.
     */
    static void setStream(std::ostream& os) {
        sStream = &os;
    }

    static const char* logLevelToColor(mve::Logger::Level level);

private:
    static int getElapsedTime();

private:
    static int              sDebugLevel;
    static bool             sLogFunctionCalls;
    static bool             sLogSourcePosition;
    static bool             sUseAnsiColorSequences;
    static sf::Clock        sClock;
    static std::ostream*    sStream;
#ifdef LOGME_USE_LOCKS
    static sf::Mutex        sMutex;
#endif

private:
    void logHeader(Level level) const;
    bool checkVerbosity(Logger::Level level) const;

private:
    const char*     mOwner;
    bool            mSilent;

};

} // namespace mve

#ifndef LOGME_DISABLED
    // Use LOGME_CALL before any other LOG_* calls.
    #define LOGME(level, ...)           do { _log_inst.debug(level, __LINE__, __FILE__, __VA_ARGS__); } while(0)
    #define LOGME_CALL(x)               mve::Logger _log_inst(x, false)
    #define LOGME_CALL_SILENT(x)        mve::Logger _log_inst(x, true)
    #ifndef LOGME_DISABLED_ASSERTS
        template <typename... Args>
        void inline logme_do_assert(bool test, mve::Logger& logger, const std::string& format, Args... args)
        {
            if (!test) {
                std::string failedMsg = "Assertion failed: " + format;
                logger.debug(mve::Logger::ERROR, __LINE__, __FILE__, failedMsg.c_str(), args...);
                throw std::runtime_error("Assertion failed");
            }
        }

        #define LOGME_ASSERT(test, ...)     do { logme_do_assert(test, _log_inst, __VA_ARGS__); } while (0)
    #endif
#else
    #define LOGME(level, ...)           do {} while(0)
    #define LOGME_CALL(x)               do {} while(0)
    #define LOGME_CALL_SILENT(x)        do {} while(0)
    #define LOGME_ASSERT(test, x, ...)  do {} while(0)
#endif

// Use these macros in the code to log at specified verbosity
#define LOGME_TRACE(...)	LOGME(mve::Logger::Level::TRACE,  __VA_ARGS__)
#define LOGME_DEBUG(...)	LOGME(mve::Logger::Level::DEBUG,  __VA_ARGS__)
#define LOGME_INFO(...)     LOGME(mve::Logger::Level::INFO,  __VA_ARGS__)
#define LOGME_WARN(...)     LOGME(mve::Logger::Level::WARN,  __VA_ARGS__)
#define LOGME_ERROR(...)    LOGME(mve::Logger::Level::ERROR,  __VA_ARGS__)
