// TODO: Cleanup the licence
// TODO: Documentation

#pragma once

#include <string>
#include <thread>

namespace mve {
    namespace Platform {

        /**
         * Returns current PID.
         */
        unsigned int getCurrentProcessId();

        /**
         * Returs current TID.
         */
        std::thread::id getCurrentThreadId();

        /**
         * Returns current working directory
         */
        std::string getCwd();
    }
}
