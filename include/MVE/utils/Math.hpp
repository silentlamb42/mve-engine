#pragma once

#include <cmath>
#include <SFML/System/Vector2.hpp>

// TODO: Documentation

namespace mve
{
    namespace math
    {

        // ------ Vector

        inline float length(const sf::Vector2f& vec)
        {
            return std::sqrt(vec.x * vec.x + vec.y * vec.y);
        }

        inline sf::Vector2f normalized(const sf::Vector2f vec)
        {
            float len = length(vec);
            return len > 0 ? (vec / len) : sf::Vector2f(0, 0);
        }

        inline float polarRotation(const sf::Vector2f vec)
        {
            return std::atan2(vec.y, vec.x);
        }



        // ------ General math

        // http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
        inline unsigned long next_pow2(unsigned long v)
        {
            v--;
            v |= v >> 1;
            v |= v >> 2;
            v |= v >> 4;
            v |= v >> 8;
            v |= v >> 16;
            v++;
            return v;
        }
    }
}
