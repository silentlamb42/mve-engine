#pragma once

#include <cstddef>
#include <utility>

// CMake handles this default by itself, guard added for non-cmake cases
#ifndef MVE_ALLOC_DEF_ALIGNMENT
    #define MVE_ALLOC_DEF_ALIGNMENT 4
#endif


namespace mve
{
    using MvSize = std::size_t;
    constexpr MvSize DEFAULT_ALIGNMENT = MVE_ALLOC_DEF_ALIGNMENT;

    namespace priv
    {


        /**
         * @return left & right both reinterpret casted to MvSize
         */
        template <typename T1, typename T2>
        inline MvSize bitAnd(const T1& left, const T2& right) {
            return reinterpret_cast<MvSize>(left) & reinterpret_cast<MvSize>(right);
        }

        /**
         * Add offset bytes to the pointer
         *
         * @param p address
         * @param x offset
         *
         * @return pointer moved by offset bytes
         */
        inline void* add(void* p, MvSize x) {
            return reinterpret_cast<void*>(reinterpret_cast<MvSize>(p) + x);
        }

        /**
         * Align given address to the given alignment value.
         *
         * @param addr      pointer to align
         * @param alignment alignment value (must be a power of 2)
         *
         * @return aligned address
         */
        inline void* align(void* addr, MvSize alignment) {
            return reinterpret_cast<void*>(bitAnd(add(addr, alignment - 1), ~(alignment -1)));
        }

        /**
         * @brief Return adjustment value in order to given address be aligned of given value
         *
         * @param addr      address to align
         * @param alignment alignment value (must be a power of 2)
         *
         * @return offset by which address needs to be moved in order to be aligned
         */
        inline MvSize align_adjustment(void* addr, MvSize alignment) {
            MvSize adjustment = alignment - bitAnd(addr, alignment - 1);
            return (adjustment == alignment) ? 0 : adjustment;
        }
    }


    class MvObjectPool
    {
    public:

        /**
         * @brief MvMvObjectPool
         * @param objectSize
         * @param objAlignment
         * @param memSize
         * @param memPtr
         */
        MvObjectPool(MvSize objSize, MvSize objAlignment, MvSize memSize, void* memPtr);

        // Non-copyable
        MvObjectPool(const MvObjectPool&) = delete;
        MvObjectPool(MvObjectPool&&) = default;

        // Movable (default impl)
        MvObjectPool& operator=(const MvObjectPool&) = delete;
        MvObjectPool& operator=(MvObjectPool&&) = default;

        ~MvObjectPool();

    public:

        void*   allocate(MvSize size, MvSize alignment = DEFAULT_ALIGNMENT);
        void    deallocate(void *p);

    private:
        const MvSize    mMemSize;
        void* const     mMemStart;

        MvSize          mObjectSize;
        MvSize          mObjectAlignment;

        void**          mFreeObjects;
    };

    namespace mem
    {

        /**
         * Acquire memory for single object and call ctor
         */
        template <typename T, typename... Args>
        T* newObject(MvObjectPool pool, Args&&... args)
        {
            // TODO: Debug mode filling this piece of memory with debug chars
            return new (pool.allocate(sizeof(T), alignof(T))) T(std::forward<Args>(args)...);
        }

        /**
         * Release memory of single object with dtor call
         */
        template <typename T>
        void deleteObject(MvObjectPool& pool, T& object)
        {
            object.~T();
            // TODO: Debug mode filling this piece of memory with debug chars
            pool.deallocate(&object);
        }
    }
}
