/**
 * Content of this file has been originally created by Alec Thomas <alec@swapoff.org>
 * for EntityX library. The API suits my needs so I've taken it and modified to match
 * MVE engine API as well as my coding standards and practices.
 *
 * Original licence:
 * ---
 *  Copyright (C) 2012 Alec Thomas
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the "Software"), to deal in
 *  the Software without restriction, including without limitation the rights to
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do
 *  so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 * ---
 *
 *  Author of the original file: Alec Thomas <alec@swapoff.org>
 *  Author of modifications: Marcin Glinski <marcin@marcinglinski.pl>
 */
#pragma once

#include <MVE/utils/Logger.hpp>

#include <SimpleSignal/simplesignal.h>

#include <functional>
#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

// TODO: Documentation
// TODO: Ifndef guard

// Helper macros:

/**
 *
 */
#define MVE_EVENT(NAME) NAME : public Event<NAME>


namespace mve
{

    /**
     * @brief The BaseEvent class
     */
    class BaseEvent
    {
    public:
        using Type = std::size_t;

    public:
        virtual ~BaseEvent() {}

    protected:
        static Type sTypeCounter;
    };


    using EventSignal = Simple::Signal<void (const void*)>;
    using EventSignalPtr = std::shared_ptr<EventSignal>;
    using EventSignalWeakPtr = std::weak_ptr<EventSignal>;

    using ConnectionKey = BaseEvent::Type;
    using ConnectionValue = std::pair<EventSignalWeakPtr, std::size_t>;


    /**
     *
     */
    template <typename Derived>
    class Event : public BaseEvent
    {
    public:

        /**
         * @brief type
         * @return
         */
        static Type type()
        {
            static Type type = sTypeCounter++;
            return type;
        }
    };

    /**
     * @brief The BaseReceiver class
     */
    class BaseReceiver
    {
    public:

        virtual ~BaseReceiver()
        {
            LOGME_CALL("BaseReceiver::dtor");
            for (auto connection : mConnections) {
                auto& ptr = connection.second.first;
                if (!ptr.expired()) {
                    ptr.lock()->disconnect(connection.second.second);
                }
            }
            LOGME_TRACE("Disconnected from all connections");
        }

    public:

        /**
         * @brief connectedSignalsNum
         * @return
         */
        std::size_t connectedSignalsNum() const
        {
            std::size_t count = 0;
            for (auto connection : mConnections) {
                auto& ptr = connection.second.first;
                if (!ptr.expired()) {
                    ++count;
                }
            }
            return count;
        }

    private:
        friend class EventManager;

        template <typename E>
        void connect(const ConnectionValue& value)
        {
            LOGME_CALL_SILENT("BaseReceiver::connect");
            mConnections.insert(std::make_pair(Event<E>::type(), value));
            LOGME_TRACE("Added new connection (event type:%d)", Event<E>::type());
        }

        std::unordered_map<ConnectionKey, ConnectionValue> mConnections;
    };

    /**
     * @brief The Receiver class
     */
    template <typename Dereived>
    class Receiver : public virtual BaseReceiver
    {
    public:
        virtual ~Receiver() {}
    };

    // TODO: non copyable
    class EventManager
    {
    public:
        EventManager() {}
        virtual ~EventManager() {}

    public:

        /**
         * @brief Subscribe receiver to receive events of type E.
         *
         * Receiver needs to be a subclass of Receiver class and needs to implement receive()
         * method accepting the given event type.
         *
         * Usage:
         *
         *     struct ExplosionReceiver : public Receiver<ExplosionReceiver>
         *     {
         *          void receive(const Explosion& explosion)
         *          {
         *              // ...
         *          }
         *     };
         *
         *     ExplosionReceiver receiver;
         *     em.subscribe<Explosion>(receiver);
         *
         */
        template <typename E, typename Receiver>
        void subscribe(Receiver& receiver)
        {
            LOGME_CALL_SILENT("EventManager::subscribe");
            void (Receiver::*receive)(const E &) = &Receiver::receive;

            auto sig = signalFor(Event<E>::type());
            auto wrapper = EventCallbackWrapper<E>(std::bind(receive, &receiver, std::placeholders::_1));
            auto connection = sig->connect(wrapper);

            BaseReceiver &base = receiver;
            // mConnection could be used directly, but it's better to hide some details

            LOGME_TRACE("Subscribed receiver to receive events of type: %d", Event<E>::type());
            base.connect<E>(std::make_pair(EventSignalWeakPtr(sig), connection));
        }

        /**
         * @brief Unsubcribe receiver from receiving events of type E.
         *
         * Note: Receiver needs to be subscribed first.
         *
         * Usage:
         *
         *     struct ExplosionReceiver : public Receiver<ExplosionReceiver>
         *     {
         *          void receive(const Explosion& explosion)
         *          {
         *              // ...
         *          }
         *     };
         *
         *     ExplosionReceiver receiver;
         *     em.subscribe<Explosion>(receiver);
         *
         *     // ...
         *
         *     em.unsubscribe<Explosion>(receiver);
         */
        template <typename E, typename Receiver>
        void unsubscribe(Receiver& receiver)
        {
            LOGME_CALL_SILENT("EventManager::unsubscribe");
            BaseReceiver& base = receiver;
            LOGME_ASSERT(base.mConnections.find(Event<E>::type()) != base.mConnections.end(),
                         "Receiver not subscribed");
            auto pair = base.mConnections[Event<E>::type()];
            auto connection = pair.second;
            auto &ptr = pair.first;
            if (!ptr.expired()) {
                ptr.lock()->disconnect(connection);
            }

            LOGME_TRACE("Unsubscribed receiver from events of type: %d", Event<E>::type());
            base.mConnections.erase(Event<E>::type());
        }

        /**
         * @brief Emit constructed event of type E to connected receivers
         */
        template <typename E>
        void send(const E& event)
        {
            LOGME_CALL_SILENT("EventManager::send");
            auto sig = signalFor(Event<E>::type());

//            LOGME_TRACE("Sending event: %s", event);

            sig->emit(&event);
        }

        /**
         * @brief Emit event to connected receivers
         *
         * Construct new Event of type E with provided arguments. Deliver the event to all
         * connected recipients.
         *
         * Usage:
         *
         *      std::shared_ptr<EventManager> em = new EventManager();
         *      em->send<Explosion>(10);
         */
        template <typename E, typename... Args>
        void send(Args && ... args)
        {
            LOGME_CALL_SILENT("EventManager::send<...>");
            E event = E(std::forward<Args>(args) ...);
            auto sig = signalFor(Event<E>::type());

//            LOGME_TRACE("Sending event: %s", event);

            sig->emit(&event);
        }

        /**
         * @brief connectedReceiversNum
         * @return
         */
        std::size_t connectedReceiversNum() const
        {
            std::size_t count = 0;
            for (EventSignalPtr handler : mHandlers) {
                if (handler) {
                    count += handler->size();
                }
            }
            return count;
        }

    private:
        EventSignalPtr& signalFor(std::size_t id)
        {
            if (id >= mHandlers.size()) {
                mHandlers.resize(id + 1);
            }
            if (!mHandlers[id]) {
                mHandlers[id] = std::make_shared<EventSignal>();
            }

            return mHandlers[id];
        }

    private:

        /**
         *
         */
        template <typename E>
        struct EventCallbackWrapper
        {
            explicit EventCallbackWrapper(std::function<void(const E&)> callback)
                : mCallback (callback)
            {
            }

            void operator()(const void *event)
            {
                mCallback(*(static_cast<const E*>(event)));
            }

        private:
            std::function<void(const E&)> mCallback;
        };

    private:
        std::vector<EventSignalPtr> mHandlers;
    };
}
