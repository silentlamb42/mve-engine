#pragma once

#include <MVE/entity/Ecs.hpp>
#include <MVE/entity/System.hpp>
#include <MVE/components/State.hpp>
#include <SFML/System/Time.hpp>

// TODO: Documentation

namespace mve
{

    template <typename T>
    class TimerSystem : public System<TimerSystem<T>, T>
    {
    public:
        static const char* name()
        {
            return "TimerSystem";
        }

    public:

        void configure(EngineContext &, EcsContext<T>& context) override;
        void update(EcsContext<T>& context, sf::Time dt) override;
    };


    template <typename T>
    void TimerSystem<T>::configure(mve::EngineContext&, EcsContext<T> &context)
    {
        // NOOP
    }


    template <typename T>
    void TimerSystem<T>::update(EcsContext<T> &context, sf::Time dt)
    {
        // TODO: add processAnyOf method to ComponentManager
        auto c = context.c();

        c->template process<State>([dt] (Entity entity, T& data)
        {
            State* cState = data.template get<State>();
            cState->updateTimer(dt);
        });
    }

}
