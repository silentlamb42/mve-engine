#pragma once

#include <MVE/components/Kinematic.hpp>
#include <MVE/components/RigidBody.hpp>
#include <MVE/components/Sensor.hpp>
#include <MVE/components/Transform.hpp>
#include <MVE/entity/Component.hpp>
#include <MVE/entity/Entity.hpp>
#include <MVE/entity/Ecs.hpp>
#include <MVE/entity/System.hpp>
#include <MVE/event/Events.hpp>
#include <MVE/logic/CollisionListener.hpp>

#include <Box2D/Box2D.h>

#include <memory>
#include <iosfwd>

class b2World;

namespace mve
{

    namespace
    {
        const b2Vec2        NoGravity(0.0f, 0.0f);

        const float         TimeStep            = 1.f / 60.f;
        constexpr int       VelocityIterations  = 10;
        constexpr int       PositionIterations  = 8;
    }

    namespace ecs
    {
        template <typename T>
        using Context = EcsContext<T>;
    }

    /**
     *
     */
    template <typename T>
    class PhysicsSystem
        : public System<PhysicsSystem<T>, T>
        , public Receiver<EntityDestroyedEvent>
        , public b2ContactListener
    {
    public:
        using WorldPtr  = std::unique_ptr<b2World>;

    public:

        /**
         * @brief Return name of this system for debuging purposes
         */
        static const char* name();

    public:

        /**
         * @brief
         *
         * @param context ECS context
         */
        void configure(EngineContext &, ecs::Context<T>& context) override;

        /**
         * @brief
         *
         * @param context   ECS context
         * @param dt        time since last update
         */
        void update(ecs::Context<T>& context, mve::DeltaTime dt) override;

        /**
         * @brief render
         */
        void render();

        /**
         * @brief setDebugDraw
         *
         * @param debugDraw
         */
        void setDebugDraw(b2Draw* debugDraw);

        /**
         * @brief sync
         * @param entity
         */
        void initialize(Entity entity);

        /**
         * TODO: Investigate issues with unregisterBody/Fixture
         *
         * @brief reset
         */
        void resetWorld();

        public:

        void setCollisionListener(CollisionListener* listener);

        /**
         * @brief This is called when two fixtures begin to overlap.
         *
         * This is called for sensors and non-sensors. This event can only occur
         * inside the time step.
         *
         * @param contact
         */
        void BeginContact(b2Contact* contact);

        /**
         * @brief This is called when two fixtures cease to overlap.
         *
         * This is called for sensors and non-sensors. This may be when a body is
         * destroyed, so this event can occur outside the time step.
         *
         * @param contact
         *
         */
        void EndContact(b2Contact* contact);

        /**
         * @brief This is called after collision detection, but before collision resolution.
         *
         * This gives you a chance to disable the contact based on the current configuration.
         * For example, you can implement a one-sided platform using this callback and calling
         * b2Contact::SetEnabled(false). The contact will be re-enabled each time through
         * collision processing, so you will need to disable the contact every time-step.
         * The pre-solve event may be fired multiple times per time step per contact due to
         * continuous collision detection.
         *
         * @param contact
         * @param oldManifold
         */
        void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);

        /**
         * @brief The post solve event is where you can gather collision impulse results.
         *
         * If you don’t care about the impulses, you should probably just implement the
         * pre-solve event.
         *
         * @param contact
         * @param impulse
         */
        void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);

    public:
        void receive(const EntityDestroyedEvent& e);

    private:
        void syncFromBox2d(T& data);
        void syncToBox2d(T& data);

    private:
        WorldPtr            mWorld;
        b2Body*             mSensorBody;
        ecs::Context<T>*    mContext;
        b2Draw*             mDebugDraw {nullptr};
        CollisionListener*  mListener {nullptr};
    };


    //
    // Methods
    //


    template <typename T>
    const char* PhysicsSystem<T>::name()
    {
        return "PhysicsSystem";
    }

    template <typename T>
    void PhysicsSystem<T>::configure(EngineContext&, ecs::Context<T>& context)
    {
        LOGME_CALL("PhysicsSystem<T>::configure");
        LOGME_INFO("Initializing physics system");

        mContext = &context;

        resetWorld();
        context.events.template subscribe<EntityDestroyedEvent>(*this);
    }

    template <typename T>
    void PhysicsSystem<T>::update(ecs::Context<T>& context, DeltaTime dt)
    {
        auto c = context.c();

        // First, upload entity state to the Box2D
        c->template process<Kinematic, RigidBody, Transform>([&] (Entity entity, T& data)
        {
            syncToBox2d(data);
        });

//        mWorld->Dump();
        mWorld->Step(TimeStep, VelocityIterations, PositionIterations);

        c->template process<Kinematic, RigidBody, Transform>([&] (Entity entity, T& data)
        {
            syncFromBox2d(data);
        });

        mWorld->ClearForces();
    }

    template <typename T>
    void PhysicsSystem<T>::render()
    {
        mWorld->DrawDebugData();
    }

    template <typename T>
    void PhysicsSystem<T>::setDebugDraw(b2Draw *debugDraw)
    {
        mDebugDraw = debugDraw;
        mDebugDraw->SetFlags(b2Draw::e_shapeBit);

        mWorld->SetDebugDraw(debugDraw);
    }

    template <typename T>
    void PhysicsSystem<T>::initialize(Entity entity)
    {
        LOGME_CALL("PhysicsSystem<T>::initialize");
        auto& data = mContext->c()->get(entity);

        RigidBody*  cRigidBody  = data.template get<RigidBody>();
        Sensor*     cSensor     = data.template get<Sensor>();
        Transform*  cTransform  = data.template get<Transform>();

        LOGME_ASSERT(nullptr != cTransform, "Entity %d must have a Transform component");

        if (nullptr != cRigidBody) {
            cRigidBody->setPosition(cTransform->getPosition());
        }

        bool hasSensor = cSensor != nullptr;
        if (hasSensor) {
            if (!cSensor->attachToEntity) {
                cSensor->registerFixture(entity, mWorld.get(), mSensorBody);
            }
        }

        if (nullptr != cRigidBody) {
            cRigidBody->registerBody(entity, *mWorld);
            if (hasSensor && cSensor->attachToEntity) {
                cSensor->registerFixture(entity, mWorld.get(), cRigidBody->body);
            }
        }
    }

    template <typename T>
    void PhysicsSystem<T>::resetWorld()
    {
        LOGME_CALL_SILENT("PhysicsSystem<T>::resetWorld");
        LOGME_INFO("World reset");
        mWorld.reset(new b2World(NoGravity));

        b2BodyDef def;
        def.type = b2_staticBody;
        def.userData = reinterpret_cast<void*>(-1); // TODO: Magic number
        mSensorBody = mWorld->CreateBody(&def);

        mWorld->SetContactListener(this);
        mWorld->SetDebugDraw(mDebugDraw);
    }

    template <typename T>
    void PhysicsSystem<T>::setCollisionListener(CollisionListener *listener)
    {
        mListener = listener;
    }

    template <typename T>
    void PhysicsSystem<T>::BeginContact(b2Contact* contact)
    {
        b2Fixture* fixA = contact->GetFixtureA();
        b2Fixture* fixB = contact->GetFixtureB();

        bool sensorA = fixA->IsSensor();
        bool sensorB = fixB->IsSensor();

        if (sensorA ^ sensorB) {
            b2Fixture* sensor = sensorA ? fixA : fixB;
            b2Fixture* object = sensorA ? fixB : fixA;

            mve::Entity eSensor = reinterpret_cast<Entity>(sensor->GetUserData());
            mve::Entity eObject = reinterpret_cast<Entity>(object->GetUserData());

            if (mListener && mListener->onSensorContactBegin(contact, eSensor, eObject)) {
                return;
            }
        }

        if (!(sensorA && sensorB))
        {
            mve::Entity eObjectA = reinterpret_cast<Entity>(fixA->GetUserData());
            mve::Entity eObjectB = reinterpret_cast<Entity>(fixB->GetUserData());

            if (mListener && mListener->onRigidBodyContactBegin(contact, eObjectA, eObjectB)) {
                return;
            }
        }
    }

    template <typename T>
    void PhysicsSystem<T>::EndContact(b2Contact* contact)
    {
        b2Fixture* fixA = contact->GetFixtureA();
        b2Fixture* fixB = contact->GetFixtureB();

        bool sensorA = fixA->IsSensor();
        bool sensorB = fixB->IsSensor();

        if (sensorA ^ sensorB) {
            b2Fixture* sensor = sensorA ? fixA : fixB;
            b2Fixture* object = sensorA ? fixB : fixA;

            mve::Entity eSensor = reinterpret_cast<Entity>(sensor->GetUserData());
            mve::Entity eObject = reinterpret_cast<Entity>(object->GetUserData());

            if (mListener && mListener->onSensorContactEnd(contact, eSensor, eObject)) {
                return;
            }
        }

        if (!(sensorA && sensorB))
        {
            mve::Entity eObjectA = reinterpret_cast<Entity>(fixA->GetUserData());
            mve::Entity eObjectB = reinterpret_cast<Entity>(fixB->GetUserData());

            if (mListener && mListener->onRigidBodyContactEnd(contact, eObjectA, eObjectB)) {
                return;
            }
        }
    }

    template <typename T>
    void PhysicsSystem<T>::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
    {
        // NOOP
    }

    template <typename T>
    void PhysicsSystem<T>::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
    {
        // NOOP
    }

    template <typename T>
    void PhysicsSystem<T>::receive(const EntityDestroyedEvent &e)
    {
        LOGME_CALL("PhysicsSystem<T>::receive<EntityDestroyed>");

        b2World* world = mWorld.get();
        b2Body* body = world->GetBodyList();
        while (body != nullptr) {
            b2Body* nextBody = body->GetNext();

            b2Fixture* fixture = body->GetFixtureList();
            while (fixture != nullptr) {
                b2Fixture* nextFixture = fixture->GetNext();

                Entity fixEntity = reinterpret_cast<Entity>(fixture->GetUserData());
                if (fixEntity == e.entity) {
                    LOGME_TRACE("Destroying b2Fixture of entity %d", fixEntity);
                    body->DestroyFixture(fixture);
                }

                fixture = nextFixture;
            }

            Entity bodyEntity = reinterpret_cast<Entity>(body->GetUserData());
            if (bodyEntity == e.entity) {
                LOGME_TRACE("Destroying b2Body of entity %d", bodyEntity);
                world->DestroyBody(body);
            }

            body = nextBody;
        }
    }

    template <typename T>
    void PhysicsSystem<T>:: syncFromBox2d(T &data)
    {
        LOGME_CALL("PhysicsSystem<T>::syncFromBox2d");
        Kinematic* cKinematic = data.template get<Kinematic>();
        RigidBody* cRigidBody = data.template get<RigidBody>();
        Transform* cTransform = data.template get<Transform>();

        LOGME_ASSERT(nullptr != cKinematic, "Kinematic component not attached");
        LOGME_ASSERT(nullptr != cRigidBody, "RigidBody component not attached");
        LOGME_ASSERT(nullptr != cTransform, "Transform component not attached");

        b2Body* body = cRigidBody->body;
        LOGME_ASSERT(nullptr != body, "Box2D body not initialized");

        cTransform->setPosition(mve::units::metricToPix(body->GetPosition()));
        cTransform->setRotation(0.f); // TODO: Rotation not supported yet
        cKinematic->setLinearVelocity(mve::units::metricToPix(body->GetLinearVelocity()));
    }

    template <typename T>
    void PhysicsSystem<T>::syncToBox2d(T &data)
    {
        LOGME_CALL("PhysicsSystem<T>::syncToBox2d");
        Kinematic* cKinematic = data.template get<Kinematic>();
        RigidBody* cRigidBody = data.template get<RigidBody>();

        LOGME_ASSERT(nullptr != cKinematic, "Kinematic component not attached");
        LOGME_ASSERT(nullptr != cRigidBody, "RigidBody component not attached");

        b2Body* body = cRigidBody->body;
        assert(nullptr != body && "Box2D body not initialized");

        // TODO: Angular impulse not supported yet
        // TODO: Linear impulse not supported yet
        // TODO: Angular velocity not supported yet

        const auto& v1 = cKinematic->getLinearVelocity();
        const auto& v2 = mve::units::pixToMetric(v1);

//        LOGME_DEBUG("[kinematic]: velocity: (%d,%d),  converted: (%d, %d)",
//                    v1.x, v1.y, v2.x, v2.y);

        body->SetAwake(true);
        body->SetLinearVelocity(mve::units::pixToMetric(cKinematic->getLinearVelocity()));
    }

}
