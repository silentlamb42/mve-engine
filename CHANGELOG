2015-09-13  Marcin Glinski <marcin@marcinglinski.pl>

    * resources: Replaced ResourceHolder and TextureManager with ResourceManager
    * general: Added documentation all over the place
    * general: Removed unused files (MvObject, MvIdGenerator, etc)

2015-09-08  Marcin Glinski <marcin@marcinglinski.pl>

    * ecs: Added timer API to the State component
    * scene: Made Director class non-copyable, movable
    * scene: Fixed bug with closing game at the start
    * scene: Fixed bug with missing Scene initialization

2015-09-07  Marcin Glinski <marcin@marcinglinski.pl>

    * scene: Added Scene and Director classes to replace current design of
             GameState/StateStack.

2015-09-06  Marcin Glinski <marcin@marcinglinski.pl>

    * memory: Object pool allocator
    * ecs: ctor/dtor initialization of components instead of reset() methods

2015-08-30  Marcin Glinski <marcin@marcinglinski.pl>

    * ecs: Cleaned up unregistering fixtures and bodies in RigidBody and Sensor classes.
    * ecs: Changed the way object is cleaned up (reset)

2015-08-29  Marcin Glinski <marcin@marcinglinski.pl>

    * general: Imported code from LD33 entry "Ghosty Game" (ref #1)
    * general: Include, enums, consants and namespaces cleanings (ref #1)
    * build: New file: CMakeLists.txt: main build script (ref #1)
    * build: New file: FindSimpleSignal.cmake: public domain signal/event library (ref #1)
    * build: New file: FindLua53.cmake: Lua scripting
    * ecs: Moved common components from LD33 to the engine. This includes Transform, State,
           Kinematic, RigidBody, Sensor and Renderable (ref #2)
    * ecs: Extracted common interfaces from RigidBody and Sensor classes: CollisionComponent keeping
           knowledge of collision constraints, and ShapeComponent keeping knowledge of what physical
           shape does the component have. For RigidBody it's a physical body of the entity; for
           Sensor it's a sensor activation area (ref #2)

