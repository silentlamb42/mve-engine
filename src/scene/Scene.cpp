#include <MVE/scene/Scene.hpp>
#include <MVE/scene/Director.hpp>

#include <SFML/Window/Event.hpp>

namespace mve
{

    Scene::Scene(Director& director, EngineContext& context)
        : mContext(context)
        , mDirector(director)
    {
        // NOOP
    }

    Scene::~Scene()
    {
        // NOOP
    }

    void Scene::initialize()
    {
        LOGME_CALL_SILENT("Scene::initialize");
        LOGME_DEBUG("Default (empty) implementation of Scene::initialize called");
    }


}
