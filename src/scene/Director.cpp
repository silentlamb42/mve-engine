#include <MVE/scene/Scene.hpp>
#include <MVE/scene/Director.hpp>
#include <MVE/core/EngineContext.hpp>
#include <SFML/Window/Event.hpp>

#include <utility>
#include <functional>
#include <cassert>

namespace mve
{

    Director::Director(EngineContext &context)
        : mContext(context)
    {
        LOGME_CALL("Director::ctor");
        auto request_push = [this] (const Request& request)
        {
            assert(request.type == RequestType::Push && "Push request required");
            this->mStack.push_back(createScene(request.stateId));
        };

        auto request_pop = [this] (const Request& request)
        {
            assert(request.type == RequestType::Pop && "Pop request required");
            this->mStack.pop_back();
        };

        auto request_clear = [this] (const Request& request)
        {
            assert(request.type == RequestType::Clear && "Clear request required");
            this->mStack.clear();
        };

        auto request_queue = [this] (const Request& request)
        {
            assert(request.type == RequestType::Queue && "Queue request required");
            if (this->mStack.empty())
            {
                mStack.push_back(createScene(request.stateId));
            }
            else
            {
                mQueue.push(createScene(request.stateId));
            }
        };

        auto request_switch = [this] (const Request& request)
        {
            assert(request.type == RequestType::Switch && "Switch request required");
            this->mStack.pop_back();
            this->mStack.push_back(createScene(request.stateId));
        };

        mHandlers.insert(std::make_pair(static_cast<MvUint>(RequestType::Clear),  request_clear));
        mHandlers.insert(std::make_pair(static_cast<MvUint>(RequestType::Pop),    request_pop));
        mHandlers.insert(std::make_pair(static_cast<MvUint>(RequestType::Push),   request_push));
        mHandlers.insert(std::make_pair(static_cast<MvUint>(RequestType::Queue),  request_queue));
        mHandlers.insert(std::make_pair(static_cast<MvUint>(RequestType::Switch), request_switch));
    }

    Director::~Director()
    {
        // NOOP
    }

    void Director::handleEvent(const sf::Event &event)
    {
        for (auto scene = mStack.rbegin(); scene != mStack.rend(); ++scene)
        {
            if (!(*scene)->handleEvent(event))
            {
                break;
            }
        }

        refresh();
    }

    void Director::render()
    {
        // Could be done using for (auto s : mSceneStack), used other syntax for consistency
        for (auto scene = mStack.begin(); scene != mStack.end(); ++scene)
        {
            (*scene)->render();
        }
    }

    void Director::update(sf::Time dt)
    {
        for (auto scene = mStack.rbegin(); scene != mStack.rend(); ++scene)
        {
            if (!(*scene)->update(dt))
            {
                break;
            }
        }

        refresh();
    }

    void Director::sceneClear()
    {
        mRequests.emplace_back(RequestType::Clear);
    }

    void Director::scenePop(TransitionType transition)
    {
        mRequests.emplace_back(RequestType::Pop, transition);
    }

    void Director::scenePush(MvSceneId sceneId, TransitionType transition)
    {
        mRequests.emplace_back(RequestType::Push, sceneId, transition);
    }

    void Director::sceneQueue(MvSceneId sceneId, TransitionType transition)
    {
        mRequests.emplace_back(RequestType::Queue, sceneId, transition);
    }

    void Director::sceneSwitch(MvSceneId sceneId, TransitionType transition)
    {
        mRequests.emplace_back(RequestType::Switch, sceneId, transition);
    }

    void Director::registerScene(MvSceneId id, FactoryFunc func)
    {
        mFactory.insert(std::make_pair(id, func));
    }

    Scene::Ptr Director::createScene(MvSceneId id)
    {
        LOGME_CALL("Director::createScene");
        LOGME_ASSERT(mFactory.find(id) != mFactory.end(), "Scene id:%d not registered", id);

        auto creator = mFactory.at(id);
        auto scene = creator();

        scene->initialize();

        return std::move(scene);
    }

    void Director::refresh()
    {
        LOGME_CALL_SILENT("Director::refresh");

        for (Request request : mRequests)
        {
            try
            {
                auto& handler = mHandlers.at(static_cast<MvUint>(request.type));
                handler(request);
            }
            catch (std::out_of_range& e)
            {
                LOGME_WARN("Handler for request id:%d not implemented",
                           static_cast<int>(request.type));
            }
        }

        mRequests.clear();

        if (mStack.empty() && !mQueue.empty())
        {
            mStack.push_back(std::move(mQueue.front()));
            mQueue.pop();
        }
    }
}
