#include <MVE/mem/MvObjectPool.hpp>
#include <MVE/utils/Logger.hpp>

namespace {
    inline void** ptr_to_memptr(void* ptr)
    {
        return reinterpret_cast<void**>(ptr);
    }
}

namespace mve
{

    MvObjectPool::MvObjectPool(MvSize objSize, MvSize objAlignment, MvSize memSize, void *memPtr)
        : mMemSize(memSize)
        , mMemStart(memPtr)
        , mObjectSize(objSize)
        , mObjectAlignment(objAlignment)
    {
        MvSize adjust = priv::align_adjustment(memPtr, mObjectAlignment);

        mFreeObjects = reinterpret_cast<void**>(priv::add(memPtr, adjust));

        // Calculate number of elements
        MvSize numObjects = (memSize - adjust) / mObjectSize;

        void** p = mFreeObjects;
        // Make linked list of free blocks
        for (MvSize i = 0; i < numObjects - 1; ++i) {
            *p = priv::add(p, mObjectSize);
            p = ptr_to_memptr(*p);
        }

        // Tail of the list
        *p = nullptr;
    }

    MvObjectPool::~MvObjectPool()
    {
        mFreeObjects = nullptr;
    }

    // Using size and alignment only because the interface might be extracted some day
    void* MvObjectPool::allocate(mve::MvSize size, mve::MvSize alignment)
    {
        LOGME_CALL_SILENT("PoolAllocator::allocate");
        // First, assertions; size and alignment must be the same for all allocations
        LOGME_ASSERT(size == mObjectSize,
            "Allocation allowed for objets of size (%d), not (%d)", mObjectSize, size);
        LOGME_ASSERT(alignment == mObjectAlignment,
            "Allocation allowed for alignment (%d) not (%d)", mObjectAlignment, alignment);

        if (nullptr == mFreeObjects) {
#if MVE_ALLOC_THROW
            throw std::bad_alloc();
#else
            return nullptr;
#endif
        }

        // Take head to the next block
        void* p = mFreeObjects;
        mFreeObjects = reinterpret_cast<void**>(*mFreeObjects);

        // It's just a pointer to the memory, no ctor is called
        return p;
    }

    void MvObjectPool::deallocate(void *p)
    {
        // Memory is free'd, add to the head
        *(ptr_to_memptr(p)) = mFreeObjects;
        mFreeObjects = ptr_to_memptr(p);
    }
}
