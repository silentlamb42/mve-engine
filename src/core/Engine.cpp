#include <MVE/core/Config.hpp>
#include <MVE/core/Engine.hpp>
#include <MVE/core/EngineContext.hpp>
#include <MVE/scene/Director.hpp>
#include <MVE/lua/LuaVM.hpp>
#include <MVE/lua/Wrappers.hpp>
#include <MVE/utils/Helpers.hpp>
#include <MVE/utils/Logger.hpp>

#include <string>
#include <assert.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Err.hpp>
#include <SFML/System/Time.hpp>

namespace {
    using namespace mve;

    // TODO: Move this constant to separate file
    const sf::Time TimePerFrame(sf::seconds(1.f / 60.f));

    const char* HelpMessage(
            "Options:\n\n"
            "    -d or --debug        set DEBUG log level\n"
            "    -t or --trace        set TRACE log level\n"
            "    -f or --file         dump logs to debug.log file\n"
            "    -h or --help         display this message");
}

namespace mve {


Engine::Engine(WindowPtr window, ContextPtr context, DirectorPtr Director)
    : mWindow(std::move(window))
    , mContext(std::move(context))
    , mDirector(std::move(Director))
    , mOpts()
{
    LOGME_CALL_SILENT("Engine::ctor");
    LOGME_DEBUG("Warming up...");
}

Engine::~Engine()
{
    LOGME_CALL_SILENT("Engine::dtor");
    LOGME_DEBUG("Cooling down...");
}

bool Engine::parseArgs(int argc, char **argv)
{
    LOGME_CALL_SILENT("Engine::parseArgs");
    LOGME_DEBUG("Parsing command line args");
    // Setup logging in debug/release environment

    Logger::Level   logLevel(Logger::NONE);
    bool            logFunctionCalls(config::debug::LogDisplayFunctionCalls);

    mOpts.toFile = false;

    // Parse arg options
    if (argc > 1)
    {
        int argNum = 1;
        while (argNum < argc)
        {
            std::string arg = std::string(argv[argNum++]);

            if (arg == "--debug" || arg == "-d")
            {
                // Mutliple log levels set
                if (logLevel != Logger::NONE)
                {
                    std::cout << HelpMessage << std::endl;
                    return false;
                }
                logLevel = Logger::DEBUG;

            }
            else if (arg == "--trace" || arg == "-t")
            {
                // Mutliple log levels set
                if (logLevel != Logger::NONE)
                {
                    std::cout << HelpMessage << std::endl;
                    return false;
                }
                logLevel = Logger::TRACE;
                logFunctionCalls = true;

            }
            else if (arg == "--file" || arg == "-f")
            {
                LOGME_WARN("Setting stream to: debug.log file");
                mOpts.toFile = true;
                mOpts.stream.open("debug.log");

            }
            else if (arg == "--help" || arg == "-h")
            {
                std::cout << HelpMessage << std::endl;
                return false;
            }
        }
    }

    if (logLevel == Logger::NONE)
    {
        logLevel = Logger::Level::LOGME_DEFAULT_LEVEL;
    }

    if (!mOpts.toFile)
    {
        Logger::setAnsiColorSequences(config::debug::LogUseAnsiColors);
    }

    Logger::setDebugLevel(logLevel);
    Logger::setLogFunctionCalls(logFunctionCalls);

    if (mOpts.toFile)
    {
        Logger::setStream(mOpts.stream);
    }

    // TODO: Investigate possiblity to have formatted SFML logs
    sf::err().rdbuf(mOpts.stream.rdbuf());

    return true;
}

void Engine::setDisplayFps(bool enabled)
{
    LOGME_CALL_SILENT("Engine::setDisplayFps");
    LOGME_DEBUG("FPS counter: %s", utils::toString(enabled));
    mDisplayFps = enabled;
}

void Engine::exec()
{
    LOGME_CALL("Engine::exec");

    LOGME_DEBUG("Initializing window");
    if(!initWindow())
    {
        throw std::runtime_error("Window initialization failed");
    }

    LOGME_DEBUG("Initializing engine state stack");
    if (!initDirector())
    {
        throw std::runtime_error("State stack initialization failed");
    }

    LOGME_DEBUG("Initializing resources");
    if (!initResources())
    {
        throw std::runtime_error("Resource initialization failed");
    }

#ifdef MVE_LUA_ENABLED
    LOGME_DEBUG("Initializing LUA scripts");
    if(!initScripts()) {
        throw std::runtime_error("Script initialization failed");
    }
#endif

    if (!initEngine()) {
        throw std::runtime_error("Engine initialization failed");
    }

    LOGME_DEBUG("Initialization success");

    sf::Clock   clock;
    sf::Time    timeSinceLastUpdate(sf::Time::Zero);
    sf::Event   event;

    sf::Text    debugFpsText;
    sf::Time    debugUpdateTime(sf::Time::Zero);
    std::size_t debugFramesCount(0);

    // TODO: Find nice font on a good licence terms for debug purposes

    sf::Font font;
    if (!font.loadFromFile("assets/GRUPO3.ttf"))
    {
        throw std::runtime_error("Cannot load font");
    }

    // TODO: Magic numbers
    debugFpsText.setFont(font);
    debugFpsText.setCharacterSize(14);
    debugFpsText.setColor(sf::Color::White);
    debugFpsText.setPosition(5.f, 5.f);
    debugFpsText.setString("");

    sf::RenderWindow& window    = getWindow();
    mve::Director&    director  = getDirector();


    // TODO: API to set pixel scale factor
    // TODO: API to have multiple views managed by Engine or some its component
//    sf::View scaledView(window.getDefaultView());
    sf::View scaledView(sf::Vector2f(160, 120), sf::Vector2f(320, 240));

    // Main loop
    onLoopStarted();
    while (window.isOpen())
    {
        window.setView(scaledView);
        sf::Time dt = clock.restart();
        timeSinceLastUpdate += dt;

        // We need to catch up with logic and input handling first
        while (timeSinceLastUpdate > TimePerFrame)
        {
            timeSinceLastUpdate -= TimePerFrame;

            // Process input
            while (window.pollEvent(event))
            {
                if (!onEventHandle(event))
                {
                    continue;
                }

                director.handleEvent(event);

                if (event.type == sf::Event::Closed)
                {
                    director.sceneClear();
                }
            }

            // Update logic by the constant time TimePerFrame
            onLogicUpdate(TimePerFrame);
            director.update(TimePerFrame);

            // No state on the stack means no client to handle the logic
            if (!director.hasScene()) {
                window.close();
            }
        }

        const bool displayFps = getDisplayFps();

        if (displayFps)
        {
            debugUpdateTime += dt;
            ++debugFramesCount;

            if (debugUpdateTime >= sf::seconds(1.f))
            {
                onFpsUpdate(debugFramesCount);
                debugFpsText.setString("FPS: " + utils::toString(debugFramesCount));
                debugUpdateTime -= sf::seconds(1.f);
                debugFramesCount = 0;
            }
        }

        window.clear(mBgColor);

        onRenderStarted();
        director.render();

        window.setView(window.getDefaultView());
        if (displayFps)
        {
            window.draw(debugFpsText);
        }

        onRenderFinished();
        window.display();
    }
    onLoopFinished();
}

void Engine::cleanup()
{
    LOGME_CALL("Engine::cleanup");
    if (mOpts.toFile) {
        mOpts.stream.close();
    }
}

bool Engine::initResources()
{
    LOGME_CALL("Engine::initResources");
    // Add engine resources here

    return onInitResources();
}

#ifdef MVE_LUA_ENABLED
bool Engine::initScripts()
{
    LOGME_CALL("Engine::initScripts");
    onInitLua();

    lua::LuaVM& vm = mContext->getLuaVM();

    LOGME_DEBUG("Registering engine LUA scripts");
    vm.registerScript("scripts/engine-debug.lua");

    if (!onInitScripts()) {
        return false;
    }

    LOGME_DEBUG("Loading LUA virtual machine");
    vm.load();

    return true;
}
#endif

bool Engine::initDirector()
{
    LOGME_CALL("Engine::initDirector");

    return onInitDirector();
}

bool Engine::initWindow()
{
    LOGME_CALL("Engine::initWindow");
    mWindow->setKeyRepeatEnabled(false);

    return onInitWindow();
}

bool Engine::initEngine()
{
    LOGME_CALL("Engine::initEngine");
    setDisplayFps(config::debug::DisplayFps);

    return true;
}

} // namespace mve
