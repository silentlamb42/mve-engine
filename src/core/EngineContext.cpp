#include <MVE/core/EngineContext.hpp>
#include <MVE/utils/Logger.hpp>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <cassert>

namespace mve
{

    // TODO: Cleanup texture holders and other stuff

EngineContext::EngineContext(sf::RenderWindow& window)
    : mWindow(&window)
    , mResourceManager {}
#ifdef MVE_LUA_ENABLED
    , mLuaVM()
#endif
{
}



#ifdef MVE_LUA_ENABLED
lua::LuaVM& EngineContext::getLuaVM()
{
    return mLuaVM;
}
#endif

}
