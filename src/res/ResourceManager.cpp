#include <MVE/res/ResourceManager.hpp>

namespace mve
{

    namespace
    {
        constexpr long HashFont     = 1;
        constexpr long HashShader   = 2;
        constexpr long HashSoundBuf = 3;
        constexpr long HashTexture  = 4;
    }

    void ResourceManager::registerFont(MvResId id, const std::string& filename)
    {
        mFontResources.insert(std::make_pair(id, Resource<sf::Font>()));

        Loader loader = {[this, id, filename] () {
            Resource<sf::Font>& res = mFontResources.at(id);
            res.load(filename);
        }, HashFont};

        mResourceLoaders.insert(std::make_pair(id, loader));
    }

    void ResourceManager::registerShader(MvResId id, const std::string& vert, const std::string& frag)
    {
        mShaderResources.insert(std::make_pair(id, Resource<sf::Shader>()));
        Loader loader = {[this, id, vert, frag] () {
            Resource<sf::Shader>& res = mShaderResources.at(id);
            res.load(vert, frag);
        }, HashFont};

        mResourceLoaders.insert(std::make_pair(id, loader));
    }

    void ResourceManager::registerSoundBuffer(MvResId id, const std::string& filename)
    {
        mSoundBufferResources.insert(std::make_pair(id, Resource<sf::SoundBuffer>()));
        Loader loader = {[this, id, filename] () {
            LOGME_CALL_SILENT("lambdaSoundBuffer");
            Resource<sf::SoundBuffer>& res = mSoundBufferResources.at(id);
            res.load(filename);
        }, HashFont};

        mResourceLoaders.insert(std::make_pair(id, loader));
    }

    void ResourceManager::registerTexture(MvResId id, const std::string& filename)
    {
        mTextureResources.insert(std::make_pair(id, Resource<sf::Texture>()));
        Loader loader = {[this, id, filename] () {
            Resource<sf::Texture>& res = mTextureResources.at(id);
            res.load(filename);
        }, HashFont};

        mResourceLoaders.insert(std::make_pair(id, loader));
    }

    void ResourceManager::load(MvResId id)
    {
        Loader& l = mResourceLoaders.at(id);
        l.loader();
    }

    void ResourceManager::unload(MvResId id)
    {
        // Won't be called often so not a big deal implementing it this way
        // Also there shouldn't be much of resources
        {
            auto iter = mSoundBufferResources.find(id);
            if (iter != mSoundBufferResources.end())
            {
                iter->second.unload();
                return;
            }
        }
        {
            auto iter = mTextureResources.find(id);
            if (iter != mTextureResources.end())
            {
                iter->second.unload();
                return;
            }
        }
        {
            auto iter = mShaderResources.find(id);
            if (iter != mShaderResources.end())
            {
                iter->second.unload();
                return;
            }
        }
        {
            auto iter = mFontResources.find(id);
            if (iter != mFontResources.end())
            {
                iter->second.unload();
                return;
            }
        }
    }

    sf::Font& ResourceManager::getFont(MvResId id)
    {
        Resource<sf::Font> res = mFontResources.at(id);
        return res.get();
    }

    sf::Shader& ResourceManager::getShader(MvResId id)
    {
        Resource<sf::Shader> res = mShaderResources.at(id);
        return res.get();
    }

    sf::SoundBuffer& ResourceManager::getSoundBuffer(MvResId id)
    {
        Resource<sf::SoundBuffer> res = mSoundBufferResources.at(id);
        return res.get();
    }

    sf::Texture& ResourceManager::getTexture(MvResId id)
    {
        Resource<sf::Texture> res = mTextureResources.at(id);
        return res.get();
    }


}
