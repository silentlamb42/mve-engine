// TODO: Cleanup the licence

#include <MVE/utils/Platform.hpp>

#include <thread>
#if defined _MSC_VER || defined WIN32 || defined _WIN32
#include <windows.h>
#else
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <limits.h>
#include <stdexcept>
#include <sstream>
#endif//#if defined _MSC_VER || defined WIN32 || defined _WIN32


std::thread::id mve::Platform::getCurrentThreadId()
{
    return std::this_thread::get_id();
}


unsigned int mve::Platform::getCurrentProcessId()
{
#if defined _MSC_VER || defined WIN32 || defined _WIN32
    return GetCurrentProcessId();
#else
    return getpid();
#endif
}


std::string mve::Platform::getCwd()
{
#if defined _MSC_VER || defined WIN32 || defined _WIN32
    TCHAR path[MAX_PATH];
    GetCurrentDirectory(MAX_PATH, path);
    return std::string(path);
#else
    char path[PATH_MAX];
    if (getcwd(path, PATH_MAX) != 0) {
        return std::string(path);
    }

    int error = errno;
    switch (error) {
        case EACCES:
            throw std::runtime_error("Access denied");

        case ENOMEM:
            throw std::runtime_error("Insufficient storage");

        default:
            std::ostringstream str;
            str << "Unrecognized error: " << error;
            throw std::runtime_error(str.str());
    }

#endif
}
