// TODO: Cleanup the licence

#include <MVE/utils/Logger.hpp>
#include <MVE/utils/Platform.hpp>
#include <MVE/utils/Helpers.hpp>

#include <stdexcept>


namespace {
    const char logLevelToString(mve::Logger::Level level) {
        return "_TDIWE"[level];
    }

#ifdef LOGME_256_COLORS
    const char* DebugLevelColors[] {"0", "38;5;8", "38;5;28", "38;5;75", "38;5;208", "38;5;196"};
    const char* FunctionCallColor = "37";
#else
    const char* DebugLevelColors[] {"0", "90", "94", "32", "33", "31"};
    const char* FunctionCallColor = "37";
#endif
#ifndef LOGME_DISABLED
    const char* DebugLevelNames[] {"None", "Trace", "Debug", "Info", "Warn", "Error"};
#endif
}


std::ostream*   mve::Logger::sStream                = &std::cout;
bool            mve::Logger::sLogFunctionCalls      = false;
bool            mve::Logger::sLogSourcePosition     = false;
bool            mve::Logger::sUseAnsiColorSequences = false;
int             mve::Logger::sDebugLevel            = Logger::Level::LOGME_DEFAULT_LEVEL;
sf::Clock       mve::Logger::sClock;
#ifdef LOGME_USE_LOCKS
sf::Mutex       mve::Logger::sMutex;
#endif

void mve::Logger::setDebugLevel(int level)
{
    LOGME_CALL_SILENT("Logger::setDebugLevel");
    sDebugLevel = level;
    LOGME_INFO("New log level: %s", DebugLevelNames[level]);

}

void mve::Logger::setAnsiColorSequences(bool enabled)
{
    LOGME_CALL_SILENT("Logger::setAnsiColorSequences");
    sUseAnsiColorSequences = enabled;
    LOGME_INFO("ANSI color sequences: %s", utils::toString(enabled));

}

void mve::Logger::setLogFunctionCalls(bool enabled)
{
    LOGME_CALL_SILENT("Logger::setLogFunctionCalls");
    sLogFunctionCalls = enabled;
    LOGME_INFO("Log function calls: %s", utils::toString(enabled));

}

void mve::Logger::setLogSourcePosition(bool enabled)
{
    sLogSourcePosition = enabled;
}

void mve::Logger::restartClock()
{
    sClock.restart();
}

const char* mve::Logger::logLevelToColor(mve::Logger::Level level)
{
    return DebugLevelColors[level];
}

int mve::Logger::getElapsedTime()
{
    return sClock.getElapsedTime().asMilliseconds();
}


void mve::Logger::logHeader(Logger::Level level) const
{
    *sStream
            << std::right << std::dec << std::setw(7) << getElapsedTime()
            << " " << std::right << std::dec << std::setw(5) << Platform::getCurrentProcessId()
            << " " << std::right << std::setw(9) << std::hex << Platform::getCurrentThreadId()
            << " " << std::left << std::dec << logLevelToString(level) << " ";
}

mve::Logger::Logger(const char* owner, bool silent)
    : mOwner(owner)
    , mSilent(silent)
{
    LOGME_LOCK;
    if (!mSilent && sLogFunctionCalls /* && checkVerbosity(TRACE) */) {
        if (sUseAnsiColorSequences) {
            *sStream << "\033[" << FunctionCallColor << "m";
        }

        logHeader(TRACE);
        *sStream << "CALL " << mOwner;

        if (sUseAnsiColorSequences) {
            *sStream << "\033[0m";
        }
        *sStream << std::endl;
    }
}


mve::Logger::~Logger()
{
    LOGME_LOCK;
    if (!mSilent && sLogFunctionCalls /* && checkVerbosity(TRACE) */) {
        if (sUseAnsiColorSequences) {
            *sStream << "\033[" << FunctionCallColor << "m";
        }

        logHeader(TRACE);
        *sStream << "RETN " << mOwner;

        if (sUseAnsiColorSequences) {
            *sStream << "\033[0m";
        }
        *sStream << std::endl;
    }
}

bool mve::Logger::checkVerbosity(Logger::Level level) const
{
    return sDebugLevel <= level;
}
