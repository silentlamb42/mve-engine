#include <MVE/utils/DebugDrawBox2D.hpp>
#include <MVE/utils/Helpers.hpp>
#include <SFML/Graphics/ConvexShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/System/Vector2.hpp>


DebugDraw::DebugDraw(sf::RenderTarget &target)
    : mTarget(target)
{
}


void DebugDraw::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
    sf::ConvexShape polygon(vertexCount);
    sf::Vector2f center;
    for(int i = 0; i < vertexCount; i++)
    {
        sf::Vector2f transformedVec = mve::units::metricToPix(vertices[i]);

        // flooring the coords to fix distorted lines on flat surfaces
        polygon.setPoint(i, sf::Vector2f(std::floor(transformedVec.x),
                                         std::floor(transformedVec.y)));
    } // they still show up though.. but less frequently

    // TODO: Get view from target and set the outline based on that
    polygon.setOutlineThickness(-0.5f);
    polygon.setFillColor(sf::Color::Transparent);
    polygon.setOutlineColor(DebugDraw::GLColorToSFML(color));

    mTarget.draw(polygon);
}

void DebugDraw::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
    sf::ConvexShape polygon(vertexCount);
    for(int i = 0; i < vertexCount; i++)
    {
        //polygon.setPoint(i, SFMLDraw::B2VecToSFVec(vertices[i]));
        sf::Vector2f transformedVec = mve::units::metricToPix(vertices[i]);
        // flooring the coords to fix distorted lines on flat surfaces
        polygon.setPoint(i, sf::Vector2f(std::floor(transformedVec.x), std::floor(transformedVec.y)));
    } // they still show up though.. but less frequently

    // TODO: Magic numbers
    //
    polygon.setOutlineThickness(-0.5f);
    polygon.setFillColor(DebugDraw::GLColorToSFML(color, 60));
    polygon.setOutlineColor(DebugDraw::GLColorToSFML(color));

    mTarget.draw(polygon);
}

void DebugDraw::DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color)
{
    const float32 pixelRadius = mve::units::metricToPix(radius);
    sf::CircleShape circle(pixelRadius);
    circle.setOrigin(pixelRadius, pixelRadius);
    circle.setPosition(mve::units::metricToPix(center));
    circle.setFillColor(sf::Color::Transparent);
    circle.setOutlineThickness(-1.f);
    circle.setOutlineColor(DebugDraw::GLColorToSFML(color));

    mTarget.draw(circle);
}

void DebugDraw::DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color)
{
    const float32 pixelRadius = mve::units::metricToPix(radius);
    sf::CircleShape circle(pixelRadius);
    circle.setOrigin(pixelRadius, pixelRadius);
    circle.setPosition(mve::units::metricToPix(center));
    circle.setFillColor(DebugDraw::GLColorToSFML(color, 60));
    circle.setOutlineThickness(1.f);
    circle.setOutlineColor(DebugDraw::GLColorToSFML(color));

    b2Vec2 endPoint = center + radius * axis;
    sf::Vertex line[2] =
    {
        sf::Vertex(mve::units::metricToPix(center), DebugDraw::GLColorToSFML(color)),
        sf::Vertex(mve::units::metricToPix(endPoint), DebugDraw::GLColorToSFML(color)),
    };

    mTarget.draw(circle);
    mTarget.draw(line, 2, sf::Lines);
}

void DebugDraw::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color)
{
    sf::Vertex line[] =
    {
        sf::Vertex(mve::units::metricToPix(p1), DebugDraw::GLColorToSFML(color)),
        sf::Vertex(mve::units::metricToPix(p2), DebugDraw::GLColorToSFML(color))
    };

    mTarget.draw(line, 2, sf::Lines);
}

void DebugDraw::DrawTransform(const b2Transform &xf)
{
    float lineLength = 0.4;
    /*b2Vec2 xAxis(b2Vec2(xf.p.x + (lineLength * xf.q.c), xf.p.y + (lineLength * xf.q.s)));*/
    b2Vec2 xAxis = xf.p + lineLength * xf.q.GetXAxis();
    sf::Vertex redLine[] =
    {
        sf::Vertex(mve::units::metricToPix(xf.p), sf::Color::Red),
        sf::Vertex(mve::units::metricToPix(xAxis), sf::Color::Red)
    };
    // You might notice that the ordinate(Y axis) points downward unlike the one in Box2D testbed
    // That's because the ordinate in SFML coordinate system points downward while the OpenGL(testbed) points upward
    /*b2Vec2 yAxis(b2Vec2(xf.p.x + (lineLength * -xf.q.s), xf.p.y + (lineLength * xf.q.c)));*/
    b2Vec2 yAxis = xf.p + lineLength * xf.q.GetYAxis();
    sf::Vertex greenLine[] =
    {
        sf::Vertex(mve::units::metricToPix(xf.p), sf::Color::Green),
        sf::Vertex(mve::units::metricToPix(yAxis), sf::Color::Green)
    };
    mTarget.draw(redLine, 2, sf::Lines);
    mTarget.draw(greenLine, 2, sf::Lines);
}
