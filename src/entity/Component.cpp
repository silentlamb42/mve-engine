#include <MVE/entity/Component.hpp>

namespace mve
{

    BaseComponent::Type BaseComponent::sTypeCounter = 0;


    BaseComponent::Type BaseComponent::count()
    {
        return sTypeCounter;
    }


    std::ostream& operator<<(std::ostream& os, const ComponentAttachedEvent& e)
    {
        os << "ComponentAttachedEvent(" << e.entity << ", " << e.component << ")";
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const ComponentDetachedEvent& e)
    {
        os << "ComponentDetachedEvent(" << e.entity << ", " << e.component << ")";
        return os;
    }

}
