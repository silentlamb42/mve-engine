#include <MVE/entity/Entity.hpp>
#include <MVE/utils/Logger.hpp>

namespace mve
{

    // TODO: Audit the code for possible bugs (I feel like there are some)
    // the code hasn't been stress tested yet

    // TODO: Use custom allocator and allocate all the stuff using it

    EntityManager::EntityManager(EventManager &eventManager)
        : mEventManager(eventManager)
    {
        for (std::size_t i = 0; i < EntityMaxNum; ++i)
        {
            mEntities[i].id = i;
            mEntities[i].next = i + 1;
        }
    }

    Entity EntityManager::create()
    {
        LOGME_CALL_SILENT("EntityManager::create");
        // TODO: Add debug guards here to make sure entity is not taken yet
        if (mFreeNode == invalid()) {
            throw std::logic_error("Cannot create: entity pool is depleted");
        }

        EntityId taken = mEntities[mFreeNode];
        mFreeNode = mEntities[mFreeNode].next;

        LOGME_TRACE("Entity %d created", taken.id);

        mEventManager.send<EntityCreatedEvent>(taken.id);

        return taken.id;
    }

    void EntityManager::destroy(Entity entity)
    {
        LOGME_CALL_SILENT("EntityManager::destroy");
        // TODO: Add debug guards here to make sure entity is taken

#ifndef NDEBUG
        Entity node = mFreeNode;
        while (node != invalid()) {
            if (node == entity) {
                LOGME_ERROR("Calling destroy on free node (id:%d)!", entity);
                return;
            }
            node = mEntities[node].next;
        }
#endif
        mEntities[entity].next = mFreeNode;
        mFreeNode = entity;

        mEventManager.send<EntityDestroyedEvent>(entity);

        LOGME_TRACE("Entity %d destroyed", entity);
    }

    Entity EntityManager::invalid()
    {
        return EntityMaxNum;
    }

    std::ostream& operator<<(std::ostream& os, const EntityCreatedEvent& e)
    {
        os << "EntityCreatedEvent(" << e.entity << ")";
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const EntityDestroyedEvent& e)
    {
        os << "EntityDestroyedEvent(" << e.entity << ")";
        return os;
    }
}
