#include <MVE/components/State.hpp>
#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif

#include <ostream>



// TODO: Cleanup Lua code

namespace mve
{
#ifdef MVE_LUA_ENABLED
    namespace
    {
        using Payload = mvcrawl::CrawlComponents;

        Payload* lua_getUserData(lua_State *L) {
            LOGME_CALL_SILENT("lua_getUserData");
            if (!lua_isuserdata(L, 1)) {
                LOGME_ERROR("User data expected");
                return nullptr;
            }

            return reinterpret_cast<Payload*>(lua_touserdata(L, 1));
        }

        int lua_getState(lua_State *L)
        {
            LOGME_CALL("lua_getState");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            lua_pushnumber(L, data->state.getState());
            return 1;
        }

        int lua_setState(lua_State *L)
        {
            LOGME_CALL("lua_setState");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!lua_isnumber(L, 2)) {
                LOGME_ERROR("Number required");
                return 0;
            }

            data->state.setState(lua_tonumber(L, 2));
            return 0;
        }

        const luaL_Reg LuaFunctions[] = {
            {"getState",    lua_getState},
            {"setState",    lua_setState},
            {nullptr, nullptr}
        };
    } // anonymous namespace

    void State::registerLuaContext(mve::lua::LuaVM &vm)
    {
        LOGME_CALL("State::registerLuaContext");
        LOGME_INFO("Lua: Registering Lua wrappers for State component");
        auto L = vm.L();

        lua_pushstring(L, "State");
        lua_newtable(L);

        luaL_setfuncs(L, LuaFunctions, 0);
        lua_settable(L, -3);
    }
#endif

    std::ostream& operator<<(std::ostream& os, const State & state)
    {
        os << "state:" << state.getState();
        return os;
    }



}
