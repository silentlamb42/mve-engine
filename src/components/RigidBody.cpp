#include <MVE/components/RigidBody.hpp>
#include <MVE/entity/Entity.hpp>
#include <MVE/core/Types.hpp>
#include <MVE/utils/Helpers.hpp>
#include <MVE/utils/Logger.hpp>

#include <Box2D/Box2D.h>

#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif



namespace mve
{
    RigidBody::RigidBody()
        : priv::CollisionComponent()
        , priv::ShapeComponent()
        , body {nullptr}
        , fixture {nullptr}
        , mWorld {nullptr}
        , mEnabled {true}
        , bodyDef {}
        , fixtureDef {}
    {
        bodyDef.allowSleep      = true;
        bodyDef.awake           = true;
        bodyDef.fixedRotation   = true;
        bodyDef.type            = b2_staticBody;
        bodyDef.userData        = reinterpret_cast<void*>(EntityManager::invalid());

        fixtureDef.density      = priv::DefaultDensity;
        fixtureDef.friction     = priv::DefaultFriction;
        fixtureDef.isSensor     = false;
        fixtureDef.restitution  = priv::DefaultRestitution;
        fixtureDef.filter.categoryBits  = mCategory;
        fixtureDef.filter.groupIndex    = mGroup;
        fixtureDef.filter.maskBits      = mMask;
        fixtureDef.userData = reinterpret_cast<void*>(EntityManager::invalid());
        fixtureDef.shape    = nullptr;
    }

    RigidBody::~RigidBody()
    {
        if (mWorld != nullptr) {
            mWorld->DestroyBody(body);
            mWorld = nullptr;
        }
    }

    void RigidBody::disableCollision()
    {
        mEnabled = false;
    }

    void RigidBody::registerBody(Entity entity, b2World& world)
    {
        LOGME_CALL("RigidBody::registerBody");
        LOGME_ASSERT(nullptr == body,               "Body already registered");
        LOGME_ASSERT(nullptr == fixtureDef.shape,   "Shape already attached");

        bodyDef.userData    = reinterpret_cast<void*>(entity);
        fixtureDef.userData = reinterpret_cast<void*>(entity);

        attachCollision(fixtureDef);
        attachShape(fixtureDef);

        body    = world.CreateBody(&bodyDef);
        fixture = body->CreateFixture(&fixtureDef);
        mWorld  = &world;
    }

    void RigidBody::setAngularDamping(float value)
    {
        bodyDef.angularDamping = value;
        if (nullptr != body) {
            body->SetAngularDamping(value);
        }
    }

    void RigidBody::setDensity(float value)
    {
        fixtureDef.density = value;
        if (nullptr != fixture && nullptr != body) {
            fixture->SetDensity(value);
            body->ResetMassData();
        }
    }

    void RigidBody::setFixedRotation(bool enabled)
    {
        bodyDef.fixedRotation = enabled;
        if (nullptr != body) {
            body->SetFixedRotation(enabled);
        }
    }

    void RigidBody::setFriction(float value)
    {
        fixtureDef.friction = value;
        if (nullptr != fixture) {
            fixture->SetFriction(value);
        }
    }

    void RigidBody::setLinearDamping(float value)
    {
        bodyDef.linearDamping = value;
        if (nullptr != body) {
            body->SetLinearDamping(value);
        }
    }

    void RigidBody::setRestitution(float value)
    {
        fixtureDef.restitution = value;
        if (nullptr != fixture) {
            fixture->SetRestitution(value);
        }
    }

    void RigidBody::setBulletMode(bool enabled)
    {
        LOGME_CALL("RigidBody::setBulletMode");
        bodyDef.bullet = enabled;
        if (nullptr != body) {
            LOGME_ASSERT(bodyDef.type == b2_dynamicBody, "Bullet mode works with dynamic bodies");
            body->SetBullet(enabled);
        }
    }

#ifdef MVE_LUA_ENABLED
    namespace
    {
        using Payload = mvcrawl::CrawlComponents;

        Payload* lua_getUserData(lua_State *L) {
            LOGME_CALL_SILENT("lua_getUserData");
            if (!lua_isuserdata(L, 1)) {
                LOGME_ERROR("User data expected");
                return nullptr;
            }

            return reinterpret_cast<Payload*>(lua_touserdata(L, 1));
        }

        int lua_enableCollision(lua_State *L)
        {
            LOGME_CALL("lua_enableCollision");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            data->rigidbody.enableCollision();
            return 0;
        }

        int lua_disableCollision(lua_State *L)
        {
            LOGME_CALL("lua_getAngularVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            data->rigidbody.disableCollision();
            return 0;
        }

        int lua_enableRotation(lua_State *L)
        {
            LOGME_CALL("lua_getAngularVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            data->rigidbody.enableRotation();
            return 0;
        }

        int lua_disableRotation(lua_State *L)
        {
            LOGME_CALL("lua_getAngularVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            data->rigidbody.disableRotation();
            return 0;
        }

        int lua_getMass(lua_State *L)
        {
            LOGME_CALL("lua_getAngularVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            lua_pushnumber(L, data->rigidbody.getMass());
            return 1;
        }

        const luaL_Reg LuaFunctions[] = {
            {"enableCollision",     lua_enableCollision},
            {"disableCollision",    lua_disableCollision},
            {"enableRotation",      lua_enableRotation},
            {"disableRotation",     lua_disableRotation},
            {"getMass",             lua_getMass},
            {nullptr, nullptr}
        };
    } // anonymous namespace

    void RigidBody::registerLuaContext(lua::LuaVM &vm)
    {
        LOGME_CALL("RigidBody::registerLuaContext");
        LOGME_INFO("Lua: Registering Lua wrappers for RigidBody component");
        auto L = vm.L();
        // {_G}, {Game}, {Component}
        lua_pushstring(L, "RigidBody"); // {Component} name
        lua_newtable(L);                // {Component} name {}

        luaL_setfuncs(L, LuaFunctions, 0);  // {Compo} name {funcs}
        lua_settable(L, -3);            // {Component}
    }
#endif

}
