#include <MVE/components/CollisionComponent.hpp>
#include <Box2D/Box2D.h>

namespace mve {
namespace priv {

    void CollisionComponent::attachCollision(b2FixtureDef &fixtureDef)
    {
        fixtureDef.filter.categoryBits  = mCategory;
        fixtureDef.filter.groupIndex    = mGroup;
        fixtureDef.filter.maskBits      = mMask;
    }

} // namespace priv
} // namespace mve
