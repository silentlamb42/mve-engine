#include <MVE/components/Transform.hpp>
#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif

#include <ostream>

namespace mve
{
#ifdef MVE_LUA_ENABLED
    namespace
    {
        using Payload = mvcrawl::CrawlComponents;

        // TODO: Move to LUA helpers
        Payload* lua_getUserData(lua_State *L)
        {
            LOGME_CALL_SILENT("lua_getUserData");
            if (!lua_isuserdata(L, 1)) {
                LOGME_ERROR("User data expected");
                return nullptr;
            }

            return reinterpret_cast<Payload*>(lua_touserdata(L, 1));
        }

        // -- args: lightPtr               --> table(x, y)
        int lua_getPosition(lua_State *L)
        {
            LOGME_CALL("lua_getPosition");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            const sf::Vector2f pos = data->transform.getPosition();
            lua_pushnumber(L, pos.x);
            lua_pushnumber(L, pos.y);
            return 2;
        }

        int lua_getRotation(lua_State *L)
        {
            LOGME_CALL("lua_getRotation");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            lua_pushnumber(L, data->transform.getRotation());
            return 1;
        }

        int lua_setPosition(lua_State *L)
        {
            LOGME_CALL("lua_getPosition");


            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!(lua_isnumber(L, 2) && lua_isnumber(L, 3))) {
                LOGME_ERROR("Number required");
                return 0;
            }

            float x = lua_tonumber(L, 2);
            float y = lua_tonumber(L, 3);

            data->transform.setPosition(sf::Vector2f(x, y));
            return 0;
        }

        int lua_setRotation(lua_State *L)
        {
            LOGME_CALL("lua_getPosition");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!lua_isnumber(L, 2)) {
                LOGME_ERROR("Number required");
                return 0;
            }

            float value = lua_tonumber(L, 2);
            data->transform.setRotation(value);

            return 0;
        }

        int lua_move(lua_State *L)
        {
            LOGME_CALL("lua_move");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!(lua_isnumber(L, 2) && lua_isnumber(L, 3))) {
                LOGME_ERROR("Number required");
                return 0;
            }

            float x = lua_tonumber(L, 2);
            float y = lua_tonumber(L, 3);

            data->transform.move(x, y);

            return 0;
        }

        int lua_rotate(lua_State *L)
        {
            LOGME_CALL("lua_rotate");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!lua_isnumber(L, 2)) {
                LOGME_ERROR("Number required");
                return 0;
            }

            float value = lua_tonumber(L, 2);
            data->transform.rotate(value);

            return 0;
        }

        const luaL_Reg LuaFunctions[] = {
            {"getPosition", lua_getPosition},
            {"getRotation", lua_getRotation},
            {"setPosition", lua_setPosition},
            {"setRotation", lua_setRotation},
            {"move",        lua_move},
            {"rotate",      lua_rotate},
            {nullptr, nullptr}
        };
    } // anonymous namespace

    void Transform::registerLuaContext(mve::lua::LuaVM &vm)
    {
        LOGME_CALL("Transform::registerLuaContext");
        LOGME_INFO("Lua: Registering Lua wrappers for Transform component");
        auto L = vm.L();
        // {_G}, {Game}, {Component}
        lua_pushstring(L, "Transform"); // {Component} name
        lua_newtable(L);                // {Component} name {}

        luaL_setfuncs(L, LuaFunctions, 0);  // {Compo} name {funcs}
        lua_settable(L, -3);            // {Component}
    }
#endif

    // TODO: Move this class to more suitable file
    std::ostream& operator<<(std::ostream& os, const Vector2f &vec)
    {
        os << "(" << vec.x << "," << vec.y << ")";
        return os;
    }

    std::ostream &operator<<(std::ostream &os, const Transform &t)
    {
        os << "{"
           << "pos: " << t.getPosition()
           << ", rot: " << t.getRotation()
           << ", origin: " << t.getOrigin()
           << "}";
        return os;
    }


} // namespace mvcrawl
