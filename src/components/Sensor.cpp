#include <MVE/components/Sensor.hpp>
#include <MVE/utils/Helpers.hpp>

#include <Box2D/Box2D.h>

namespace mve
{

    void Sensor::markSeen(mve::Entity entity)
    {
        LOGME_CALL_SILENT("Sensor::markSeen");
        LOGME_DEBUG("Seeing: %d", entity);
        auto found = std::find(seen.begin(), seen.end(), entity);
        if (found == seen.end()) {
            seen.push_back(entity);
        }
    }

    void Sensor::markUnseen(mve::Entity entity)
    {
        LOGME_CALL_SILENT("Sensor::markUnseen");
        LOGME_DEBUG("Not seeing: %d", entity);
        seen.erase(std::remove_if(seen.begin(), seen.end(),
        [entity] (const mve::Entity& e) {
            return entity == e;
        }), seen.end());
    }

}
