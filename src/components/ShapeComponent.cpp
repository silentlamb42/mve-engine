#include <MVE/components/ShapeComponent.hpp>


namespace mve {
namespace priv {

void ShapeComponent::setShapeBox(float w_px, float h_px, const sf::Vector2f &offset_px, float angle)
{
    b2Vec2 offsetMetric(
                pixToMetric(offset_px.x),
                pixToMetric(offset_px.y));
    shape_polygon.SetAsBox(
                pixToMetric(w_px / 2),
                pixToMetric(h_px / 2),
                offsetMetric,
                angle);
    shape_type = ShapeType::Box;
}

void ShapeComponent::setShapeCircle(float r_px, const sf::Vector2f &offset_px)
{
    shape_circle.m_p.Set(pixToMetric(offset_px.x),
                         pixToMetric(offset_px.y));
    shape_circle.m_radius = pixToMetric(r_px);
    shape_type = ShapeType::Circle;
}

void ShapeComponent::setShapePolygon(std::vector<sf::Vector2f> points_px, std::size_t count)
{
    std::vector<b2Vec2> pointsMetric;
    for (auto& point_px : points_px) {
        pointsMetric.emplace_back(pixToMetric(point_px.x),
                                  pixToMetric(point_px.y));
    }
    shape_polygon.Set(&pointsMetric[0], count);
    shape_type = ShapeType::Polygon;
}

void ShapeComponent::attachShape(b2FixtureDef &fixtureDef)
{
    switch (shape_type)
    {
        case priv::ShapeType::Box:
        case priv::ShapeType::Polygon:
            fixtureDef.shape = &shape_polygon;
            break;

        case priv::ShapeType::Circle:
            fixtureDef.shape = &shape_circle;
            break;

        default:
            throw std::logic_error("Unsupported shape type");
    }
}



} // namespace priv
} // namespace mve

