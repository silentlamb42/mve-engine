#include <MVE/components/Kinematic.hpp>

#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif


namespace mve
{

    Kinematic::Kinematic()
        : mDirection {def::direction::Unknown}
        , mImpulseAngular {0.f}
        , mVelocityAngular {0.f}
        , mImpulseLinear {}
        , mVelocityLinear {}
    {
        // NOOP
    }

    Kinematic::~Kinematic()
    {
        // NOOOP
    }

#ifdef MVE_LUA_ENABLED
    namespace
    {
        using Payload = mvcrawl::CrawlComponents;

        Payload* lua_getUserData(lua_State *L) {
            LOGME_CALL_SILENT("lua_getUserData");
            if (!lua_isuserdata(L, 1)) {
                LOGME_ERROR("User data expected");
                return nullptr;
            }

            return reinterpret_cast<Payload*>(lua_touserdata(L, 1));
        }

        // -- args: lightPtr               --> float
        int lua_getAngularVelocity(lua_State *L)
        {
            LOGME_CALL("lua_getAngularVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            lua_pushnumber(L, data->kinematic.getAngularVelocity());
            return 1;
        }

        // -- args: lightPtr               --> x, y
        int lua_getLinearVelocity(lua_State *L)
        {
            LOGME_CALL("lua_getLinearVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            sf::Vector2f v = data->kinematic.getLinearVelocity();
            lua_pushnumber(L, v.x);
            lua_pushnumber(L, v.y);
            return 2;
        }

        // -- args: lightPtr, float        --> nil
        int lua_setAngularVelocity(lua_State *L)
        {
            LOGME_CALL("lua_getAngularVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!lua_isnumber(L, 2)) {
                LOGME_ERROR("Number required");
                return 0;
            }

            float value = lua_tonumber(L, 2);
            data->kinematic.setAngularVelocity(value);

            return 0;
        }

        int lua_setLinearVelocity(lua_State *L)
        {
            LOGME_CALL("lua_setLinearVelocity");

            Payload* data = lua_getUserData(L);
            if (nullptr == data) {
                return 0;
            }

            if (!(lua_isnumber(L, 2) && lua_isnumber(L, 3))) {
                LOGME_ERROR("Number required");
                return 0;
            }

            float x = lua_tonumber(L, 2);
            float y = lua_tonumber(L, 3);
            data->kinematic.setLinearVelocity(sf::Vector2f(x, y));

            return 0;
        }

        const luaL_Reg LuaFunctions[] = {
            {"getAngularVelocity",      lua_getAngularVelocity},
            {"getLinearVelocity",       lua_getLinearVelocity},
            {"setAngularVelocity",      lua_setAngularVelocity},
            {"setLinearVelocity",       lua_setLinearVelocity},
            {nullptr, nullptr}
        };

    } // anonymous namespace

    void Kinematic::registerLuaContext(mve::lua::LuaVM &vm)
    {
        LOGME_CALL("Kinematic::registerLuaContext");
        LOGME_INFO("Lua: Registering Lua wrappers for Kinematic component");
        auto L = vm.L();
        // {_G}, {Game}, {Component}
        lua_pushstring(L, "Kinematic"); // {Component} name
        lua_newtable(L);                // {Component} name {}

        luaL_setfuncs(L, LuaFunctions, 0);  // {Compo} name {funcs}
        lua_settable(L, -3);            // {Component}
    }
#endif // MVE_LUA_ENABLED

}
