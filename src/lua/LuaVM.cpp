#ifdef MVE_LUA_ENABLED

#include <MVE/lua/LuaVM.hpp>
#include <MVE/lua/Wrappers.hpp>
#include <MVE/utils/Logger.hpp>
#include <lua.hpp>
#include <functional>
#include <type_traits>

namespace mve {
namespace lua {



    namespace {
        struct LuaLibrary
        {
            std::function<int(lua_State*)> function;
            const char* name;
        };

        // Enable configuration option for libs below

        luaL_Reg LuaLibraries[] = {
            {"_G", luaopen_base},
//            {LUA_BITLIBNAME,    luaopen_bit32},
//            {LUA_DBLIBNAME,     luaopen_debug},
//            {LUA_IOLIBNAME,     luaopen_io},
//            {LUA_MATHLIBNAME,   luaopen_math},
            {LUA_STRLIBNAME,    luaopen_string},
            // {LUA_LOADLIBNAME,    luaopen_package},
//            {LUA_TABLIBNAME,    luaopen_table},
        };

        int mvlua_print(lua_State *L)
        {
            LOGME_CALL_SILENT("[LuaScript]");
            int nargs = lua_gettop(L);
            for (int i = 1; i <= nargs; ++i) {
                if (lua_isstring(L, i)) {
                    LOGME_DEBUG("%s", lua_tostring(L, i));
                } else {
                    LOGME_WARN("passed arg is not a string");
                }
            }
            return 0;
        }

        const luaL_Reg mvelib[] = {
            {"print",  mvlua_print},
            {nullptr, nullptr}
        };

        int luaopen_mvelib(lua_State *L) {
            lua_getglobal(L, "_G");
            luaL_setfuncs(L, mvelib, 0);
            return 1;
        }
    }

    /**
     *
     */
    template <>
    int getFromStack(lua_State* L)
    {
        if (!lua_isnumber(L, -1)) {
            return lua_tointeger(L, -1);
        } else {
            throw LuaException("Value on the stack is not a number");
        }
        return 0;
    }

    /**
     *
     */
    template <>
    bool getFromStack(lua_State* L)
    {
        if (lua_isboolean(L, -1)) {
            return lua_toboolean(L, -1);
        }
        if (lua_isnumber(L, -1)) {
            return static_cast<bool>(lua_tointeger(L, -1));
        }

        throw LuaException("Value on the stack cannot be cast to a bool");
    }

    LuaVM::LuaVM()
        : mL(luaL_newstate())
        , mScripts()
    {
        init();
    }

    LuaVM::LuaVM(LuaVM &&other)
        : mL(other.mL)
        , mScripts(std::move(other.mScripts))
    {
    }

    LuaVM::~LuaVM()
    {
        lua_close(mL);
    }

    void LuaVM::init()
    {
        LOGME_CALL("LuaVM::init");
        LOGME_INFO("Loading builtin libraries");

        const std::size_t count = std::extent<decltype(LuaLibraries)>::value;
        for (std::size_t i = 0; i < count; ++i) {
            luaL_requiref(mL, LuaLibraries[i].name, LuaLibraries[i].func, 1);
            lua_pop(mL, 1);  /* remove lib from the stack */
        }

        luaopen_mvelib(mL);
        initialize_data_table(*this);
    }

    void LuaVM::load()
    {
        LOGME_CALL("LuaVM::load");
        for (const auto& script : mScripts) {
            LOGME_INFO("Loading script %s", script);
            int state = luaL_dofile(mL, script);
            logErrors(state);
        }
    }

    void LuaVM::reload()
    {
        // Shutdown existing VM
        lua_close(mL);

        // Initialize new one
        mL = luaL_newstate();
        init();
        load();
    }

    void LuaVM::registerScript(const char *filename)
    {
        mScripts.push_back(filename);
    }

    void LuaVM::createGlobalTable(const char *name, bool push)
    {
        lua_newtable(mL);
        lua_setglobal(mL, name);
        if (push) {
            pushGlobalTable(name);
        }
    }

    void LuaVM::createTable(const char *name, LuaVM::TableCallback cb)
    {
        lua_pushstring(mL, name);   // table{} name
        lua_newtable(mL);           // table{} name {}
        if (cb) {
            cb(*this);
        }
        lua_settable(mL, -3);       // table{}
    }

    void LuaVM::logErrors(int state)
    {
        LOGME_CALL_SILENT("LuaVM::logErrors");
        if (state != LUA_OK && !lua_isnil(mL, -1)) {
            const char *msg = lua_tostring(mL, -1);
            if (nullptr == msg) {
                LOGME_ERROR("[Lua] Error object is not a string");
                return;
            }

            LOGME_ERROR("[Lua] Error: %s", msg);
            lua_pop(mL, 1);
        }
    }

} /* namespace lua */
} /* namespace mve */

#endif // MVE_LUA_ENABLED
