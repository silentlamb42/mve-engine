#ifdef MVE_LUA_ENABLED
#include <MVE/lua/LuaVM.hpp>
#include <MVE/lua/Wrappers.hpp>
#include <MVE/render/MvAnimation.hpp>
#include <MVE/render/MvDrawable.hpp>
#include <MVE/entity/Component.hpp>

// Redesign blueprints

namespace {

    const char* LuaTable_Id = "Id";
    const char* LuaTable_Id_Texture = "Texture";
    const char* LuaTable_Id_Render = "Render";
    const char* LuaTable_Id_Animation = "Animation";

    const char* LuaTable_Entity = "Entity";
    const char* LuaTable_Entity_Component = "Component";
    const char* LuaTable_Entity_Type = "Type";
    const char* LuaTable_Entity_Book = "Book";
}


namespace mve {
namespace lua {

    void initialize_data_table(LuaVM &vm)
    {
        LOGME_CALL("Wrappers::initialize_data_table");
        LOGME_INFO("Creating LUA global data tables");

        vm.createGlobalTable(LuaTable_Id);

        LOGME_INFO("Registering engine enums and structures in LUA data tables");

        vm.createTable(LuaTable_Id_Animation, [] (LuaVM& vm) {
            MvAnimation::registerLuaContext(vm);
        });
        vm.createTable(LuaTable_Id_Render, [] (LuaVM& vm) {
            MvDrawable::initDataTable(vm);
        });

        vm.createTable(LuaTable_Id_Texture);    // filled by Lua

        vm.popStack();

        vm.createGlobalTable(LuaTable_Entity);

        vm.createTable(LuaTable_Entity_Component, [] (LuaVM& vm) {
//            component::initDataTable(vm);
        });

        vm.createTable(LuaTable_Entity_Book);   // filled by Lua
        vm.createTable(LuaTable_Entity_Type);   // filled by Lua

        vm.popStack();
    }

}} // namespaces
#endif // MVE_LUA_ENABLED
