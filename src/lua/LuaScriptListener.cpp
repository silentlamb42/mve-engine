#ifdef MVE_LUA_ENABLED
#include <MVE/lua/LuaScriptListener.hpp>

namespace mve
{

    LuaScriptListener::LuaScriptListener(Callback onLoad, Callback onReload)
        : mOnLoadCallback(onLoad)
        , mOnReloadCallback(onReload)
    {
    }

    void LuaScriptListener::onScriptLoad()
    {
        mOnLoadCallback();
    }

    void LuaScriptListener::onScriptReload()
    {
        mOnReloadCallback();
    }

}

#endif
