#include <MVE/render/StateDrawable.hpp>

namespace mve
{

    void StateDrawable::setState(MvStateId state)
    {
        LOGME_CALL_SILENT("StateDrawable::setState");
        if (mCurrentMvStateId == state) {
            return;
        }

        mCurrentMvStateId = state;
        LOGME_DEBUG("State changed (id:%d)", state);
    }

    void StateDrawable::onPositionChanged(const sf::Vector2f &pos)
    {
        for (auto& state : mDrawableStates) {
            state.second->setPosition(pos);
        }
    }

    void StateDrawable::flipX()
    {
        for (auto& state : mDrawableStates) {
            state.second->flipX();
        }
    }

}
