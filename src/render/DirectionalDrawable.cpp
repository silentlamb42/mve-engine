#include <MVE/render/DirectionalDrawable.hpp>

namespace mve
{

    void DirectionalDrawable::setDrawable(MvStateId state, MvDirection direction,
                                          MvDrawable::Ptr drawable)
    {
        LOGME_CALL_SILENT("DirectionalDrawable::setDrawable");
#if 0
        LOGME_DEBUG("Setting drawable for state:%d, direction:%d, rawstate:%d",
                    state, direction, getDirectionalState(state, direction));
#endif
        StateDrawable::setDrawable(getDirectionalState(state, direction), std::move(drawable));
    }

    void DirectionalDrawable::draw(sf::RenderTarget &target, sf::RenderStates states)
    {
        getDrawable(getDirectionalState(getCurrentStateId(), mDirection)).draw(target, states);
    }

    void DirectionalDrawable::update(sf::Time dt)
    {
        getDrawable(getDirectionalState(getCurrentStateId(), mDirection)).update(dt);
    }

}
