#include <MVE/render/StaticDrawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <cmath>

namespace mve
{
    StaticDrawable::StaticDrawable(EngineContext& context, MvId texId, sf::IntRect texRectangle)
    {
        sf::Texture& texture = context.getResourceManager().getTexture(texId);
        mSprite.setTexture(texture);
        mSprite.setTextureRect(texRectangle);

        sf::FloatRect bounds = mSprite.getLocalBounds();
        mSprite.setOrigin(std::floor(bounds.width / 2.f), std::floor(bounds.height / 2.f));
    }

    void StaticDrawable::draw(sf::RenderTarget &target, sf::RenderStates states)
    {
        LOGME_CALL_SILENT("StaticDrawable::draw");
        target.draw(mSprite, states);
    }

    void StaticDrawable::update(sf::Time dt)
    {
        // NOOP
    }

    void StaticDrawable::onPositionChanged(const sf::Vector2f &pos)
    {
        LOGME_CALL_SILENT("StaticDrawable::onPositionChanged");
        mSprite.setPosition(pos);
    }

    void StaticDrawable::flipX()
    {
        LOGME_CALL("StaticDrawable::flipX");
        // First flip, then compare
        if ((mFlipped = !mFlipped)) {
            LOGME_DEBUG("setScale -1, 1");
            mSprite.setScale(-1, 1);
        } else {
            LOGME_DEBUG("setScale: 1, 1");
            mSprite.setScale(1, 1);
        }
    }
}
