#include <MVE/render/MvDrawable.hpp>
#include <MVE/utils/Logger.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <type_traits>

#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif

// TODO: API to add custom render types

namespace
{
#ifdef MVE_LUA_ENABLED
    struct Descr
    {
        unsigned int id;
        const char* key;
        const char* name;
        const char* descr;
    };

    const Descr RenderTypes[] = {
        {1, "Static", "Static drawable", "Simple static image"},
        {2, "Animation", "Animation drawable", "Set of drawables to display sequentialy"},
        {3, "State", "State drawable", "Set of drawables to display depending on the state"},
    };

    const char* LuaTable_Field_Id = "id";
    const char* LuaTable_Field_Name = "name";
    const char* LuaTable_Field_Description = "description";
#endif // MVE_LUA_ENABLED
}

namespace mve
{
#ifdef MVE_LUA_ENABLED
    // TODO: Rename to registerLuaContext
    void MvDrawable::initDataTable(lua::LuaVM &vm)
    {
        LOGME_CALL("MvDrawable::initDataTable");

        LOGME_INFO("Lua: Registering available Render types");

        // Render{}
        std::size_t count = std::extent<decltype(RenderTypes)>::value;
        for (std::size_t i = 0; i < count; ++i) {
            vm.createTable(RenderTypes[i].key, [i] (lua::LuaVM& vm) {
                vm.addTableField(LuaTable_Field_Id,             RenderTypes[i].id);
                vm.addTableField(LuaTable_Field_Name,           RenderTypes[i].name);
                vm.addTableField(LuaTable_Field_Description,    RenderTypes[i].descr);
            });
        }
    }
#endif

    MvDrawable::~MvDrawable()
    {
        // NOOP
    }

    void MvDrawable::setPosition(float x, float y)
    {
        sf::Transformable::setPosition(x, y);
        onPositionChanged(sf::Vector2f(x, y));
    }

    void MvDrawable::setPosition(const sf::Vector2f &position)
    {
        sf::Transformable::setPosition(position);
        onPositionChanged(position);
    }

    void MvDrawable::draw(sf::RenderTarget &target, sf::RenderStates state)
    {
        LOGME_CALL_SILENT("MvDrawable::draw");
        LOGME_WARN("Base implmentation called!");
        // By default: draw nothing
    }

    void MvDrawable::update(sf::Time dt)
    {
        // By default: do not update
    }

    void MvDrawable::onPositionChanged(const sf::Vector2f &pos)
    {
        LOGME_CALL_SILENT("MvDrawable::onPositionChanged");
        LOGME_WARN("Default (empty) implementation of virtual method called");
    }

    void MvDrawable::flipX()
    {
        // By default: do not flip
    }

    void MvDrawable::flipY()
    {
        // By default: do not flip
    }
}
