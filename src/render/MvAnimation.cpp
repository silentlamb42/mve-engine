#include <MVE/render/MvAnimation.hpp>
#include <MVE/utils/Logger.hpp>
#ifdef MVE_LUA_ENABLED
    #include <MVE/lua/LuaVM.hpp>
#endif

// TODO: Redesign blueprints

namespace mve
{
#ifdef MVE_LUA_ENABLED
    namespace {

        struct Descr {
            const char* name;
            const char* descr;
        };

        const char* LuaTable_RepeatMode = "RepeatMode";
        const char* LuaTable_Field_Id = "id";
        const char* LuaTable_Field_Name = "name";
        const char* LuaTable_Field_Description = "description";

        const Descr RepeatModes[] {
            {"Once", "Play once and stop"},
            {"Loop", "Play infinitely"},
        };
    }

    void MvAnimation::registerLuaContext(lua::LuaVM &vm)
    {
        LOGME_CALL("MvDrawable::registerLuaContext");
        LOGME_INFO("Lua: Registering available Animation repeat modes");
        // G{} Animation{}
        vm.createTable(LuaTable_RepeatMode, [] (lua::LuaVM& vm) {
            const std::size_t repeatSize = std::extent<decltype(RepeatModes)>::value;
            for (std::size_t i = 0; i < repeatSize; ++i) {
                vm.createTable(RepeatModes[i].name, [i] (lua::LuaVM& vm) {
                    std::stringstream luaName;
                    luaName << LuaTable_RepeatMode << ":" << RepeatModes[i].name;

                    vm.addTableField(LuaTable_Field_Id, i);
                    vm.addTableField(LuaTable_Field_Name, luaName.str());
                    vm.addTableField(LuaTable_Field_Description, RepeatModes[i].descr);
                });
            }
        });
    }
#endif

    MvAnimation::MvAnimation(RepeatMode mode)
        : mRepeatMode {mode}
    {

    }

    void MvAnimation::addFrame(MvId drawableId, sf::Time duration)
    {
        LOGME_CALL("MvAnimation::addFrame");

        mAnimationLength += duration;
        mFrames.emplace_back(drawableId, mAnimationLength);
        LOGME_DEBUG("Adding frame [drawable: %d], total: %d", drawableId, mFrames.size());
    }

    void MvAnimation::addDrawable(MvDrawable::Ptr drawable)
    {
        LOGME_CALL("MvAnimation::addFrameDef");
        mDrawables.emplace_back(std::move(drawable));
        LOGME_DEBUG("Adding drawable, total: %d", mDrawables.size());
    }

    void MvAnimation::draw(sf::RenderTarget &target, sf::RenderStates state)
    {
        FrameSpec& frameSpec = mFrames[mCurFrameIndex];
        if (!mFinished) {
            mDrawables[frameSpec.frame]->draw(target, state);
        }
    }

    void MvAnimation::flipX()
    {
        for (auto& drawable : mDrawables) {
            drawable->flipX();
        }
    }

    void MvAnimation::update(sf::Time dt)
    {
        LOGME_CALL_SILENT("MvAnimation::update");
        if (mFinished) {
            return;
        }

        mCurFrameTimer += dt;
        if (mCurFrameTimer >= mAnimationLength) {
            mCurFrameTimer -= mAnimationLength;
            mCurFrameIndex = 0;

            if (mRepeatMode == RepeatMode::Once) {
                mFinished = true;
            }
        }

        // Find new frame to render based on the elapsed time from the previous update
        while (mCurFrameTimer >= mFrames[mCurFrameIndex].endPos) {
            ++mCurFrameIndex;
            LOGME_ASSERT(mCurFrameIndex < mFrames.size(),
                         "curFrame index out of bounds (index:%d, size:%d)",
                         mCurFrameIndex, mFrames.size());
        }
    }

    void MvAnimation::onPositionChanged(const sf::Vector2f &pos)
    {
        for (auto& drawable : mDrawables) {
            drawable->setPosition(pos);
        }
    }

}
