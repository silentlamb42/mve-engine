#
# FindSimpleSignal.cmake
# Author: Marcin Glinski <marcin@marcinglinski.pl>
#
# This module tries to find the SimpleSignal headers and sets the following
# variables:
#     - SIMPLESIGNAL_INCLUDE_DIR
#     - SIMPLESIGNAL_FOUND
#
# It also uses following variables as a lookup paths:
#     - SIMPLESIGNAL_ROOT
#

set(FIND_SIMPLESIGNAL_PATHS
    ${SIMPLESIGNAL_ROOT}
    $ENV{SIMPLESIGNAL_ROOT}
    ${CMAKE_SOURCE_DIR}/ext/include
    ~/Libs/Cpp
    ~/Libraries
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local
    /usr
    /sw
    /opt/local
    /opt/csw
    /opt
)

find_path(SIMPLESIGNAL_INCLUDE_DIR  SimpleSignal/simplesignal.h
    PATH_SUFFIXES
    PATHS ${FIND_SIMPLESIGNAL_PATHS})


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SIMPLESIGNAL DEFAULT_MSG SIMPLESIGNAL_INCLUDE_DIR)
