#
# FindLua53.cmake
# Author: Marcin Glinski <marcin@marcinglinski.pl>
#
# This module tries to find the Lua 5.2 library and sets the following
# variables:
#     - LUA53_INCLUDE_DIR
#     - LUA53_LIBRARY
#     - LUA53_LIBRARIES
#     - LUA53_FOUND
#
# It also uses following variables as a lookup paths:
#     - LUA53_ROOT
#

set(FIND_LUA53_PATHS
    ${LUA53_ROOT}
    ${LUA53_ROOT}/src
    $ENV{LUA53_ROOT}
    ~/Libs/Cpp
    ~/Libraries
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local
    /usr
    /sw
    /opt/local
    /opt/csw
    /opt
)

find_path(LUA53_INCLUDE_DIR lua.h
    PATH_SUFFIXES src
    PATHS ${FIND_LUA53_PATHS})

find_library(LUA53_LIBRARY
  NAMES lua
  PATHS ${FIND_LUA53_PATHS})

if(LUA53_LIBRARY)
    find_library(LUA53_MATH_LIBRARY m)
    find_library(LUA53_DL_LIBRARY dl)
    set(LUA53_LIBRARIES ${LUA53_LIBRARY} ${LUA53_MATH_LIBRARY} ${LUA53_DL_LIBRARY} CACHE STRING "Lua Libraries")
endif(LUA53_LIBRARY)

# message(STATUS "FindLua53:")
# message(STATUS "  * Library  : ${LUA53_LIBRARY}")
# message(STATUS "  * Libraries: ${LUA53_LIBRARIES}")
# message(STATUS "  * Includes : ${LUA53_INCLUDE_DIR}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Lua53 DEFAULT_MSG LUA53_LIBRARIES LUA53_INCLUDE_DIR)
