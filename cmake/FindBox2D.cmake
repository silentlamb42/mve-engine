#
# FindBox2D.cmake
# Author: Marcin Glinski <marcin@marcinglinski.pl>
#
# This module tries to find the Box2D library and sets the following
# variables:
#     - BOX2D_INCLUDE_DIR
#     - BOX2D_LIBRARY
#     - BOX2D_FOUND
#
# It also uses following variables as a lookup paths:
#    - BOX2D_ROOT
#


if(BOX2D_INCLUDE_DIR)
    set(BOX2D_FIND_QUIETLY TRUE)
endif(BOX2D_INCLUDE_DIR)


set(FIND_BOX2D_PATHS
    ${BOX2D_ROOT}
    ${BOX2D_ROOT}/Box2D
    ${BOX2D_ROOT}/Build/Box2D
    $ENV{BOX2D_ROOT}
    ~/Libs/Cpp
    ~/Libraries
    ~/Library/Frameworks
    /Library/Frameworks
    /usr/local
    /usr
    /sw
    /opt/local
    /opt/csw
    /opt
)

find_path(BOX2D_INCLUDE_DIR Box2D/Box2D.h
                PATHS ${FIND_BOX2D_PATHS})

find_library(BOX2D_LIBRARY_DEBUG
        NAMES Box2D box2d box2d box2D
        PATH_SUFFIXES lib64 lib Debug Build/Debug
        PATHS ${FIND_BOX2D_PATHS})

find_library(BOX2D_LIBRARY_RELEASE
                NAMES Box2D box2d Box2d box2D
                PATHS ${FIND_BOX2D_PATHS})

if(CMAKE_BUILD_TYPE MATCHES Debug)
    set(BOX2D_LIBRARY	${BOX2D_LIBRARY_DEBUG})
elseif(CMAKE_BUILD_TYPE MATCHES Release)
    set(BOX2D_LIBRARY	${BOX2D_LIBRARY_RELEASE})
else(CMAKE_BUILD_TYPE)
    set(BOX2D_LIBRARY   ${BOX2D_LIBRARY_RELEASE})
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Box2D DEFAULT_MSG BOX2D_LIBRARY BOX2D_INCLUDE_DIR)

